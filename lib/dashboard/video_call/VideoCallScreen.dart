
import 'package:countdown_flutter/countdown_flutter.dart';
import 'package:dating_app/utils/ColorUtils.dart';
import 'package:dating_app/utils/WidgetUtils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class VideoCallScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return VideoCallScreenState();
  }
  
}
class VideoCallScreenState extends State<VideoCallScreen>{
  @override
  Widget build(BuildContext context) {

      return Scaffold(

        body: Container(
            child:   Stack(
              children: [
                Container(
                  width: double.infinity,
                  height: double.infinity,

                  decoration: BoxDecoration(

                      image: DecorationImage(
                        fit: BoxFit.cover,
                          image: AssetImage("assets/girls/img_1.jpeg")
                      )
                  ),
                ),


             /*  Align(
                 widthFactor: 50,
                 alignment: Alignment.topRight,
                 child:
               ),*/
                Positioned(
                    top: 20,
                    right: 10,


                    child: Container(

                      width: 200,
                      padding: EdgeInsets.all(20),
                      height: 300,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          image: DecorationImage(
                              fit: BoxFit.fill,
                              image: AssetImage("assets/img/ic_user.jpg")
                          )
                      ),
                    )),
                Align(
                  alignment: Alignment.topLeft,

                  child:    InkWell(
                    onTap: (){
                      Navigator.pop(context,null);
                    },
                    child: Container(
                      margin: EdgeInsets.only(top: 20),
                        alignment: Alignment.center,
                        width: 60,
                        height: 60,
                        padding: EdgeInsets.all(15),

                        child: Icon(Icons.arrow_back_ios_outlined, color: Colors.white, size: 24,)
                    )
                  ),
                ),
                Align(

                  alignment: Alignment.bottomCenter,
                  child: Container(

                    margin: EdgeInsets.only(bottom: 20),
                    child:  Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [

                        TextButton(onPressed: () {
                        }, child: Card(

                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(80),
                            ),
                            elevation: 5,
                            color: ColorUtils.colorBflight,
                            child: Container(
                                alignment: Alignment.center,
                                width: 60,
                                height: 60,
                                padding: EdgeInsets.all(15),
                                child: Icon(
                                  Icons.volume_down, color: Colors.white, size: 24,)
                            )),
                        ),
                        TextButton(onPressed: () {
                        }, child: Card(

                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(80),
                            ),
                            elevation: 5,

                            color: ColorUtils.colorBflight,
                            child: Container(
                                alignment: Alignment.center,
                                width: 60,
                                height: 60,
                                padding: EdgeInsets.all(15),

                                child: Icon(Icons.mic, color: Colors.white, size: 24,)
                            )),
                        ),
                        TextButton(onPressed: () {


                        }, child: Card(

                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(80),
                            ),
                            elevation: 5,
                            color: ColorUtils.colorRedNew,
                            child: Container(
                                alignment: Alignment.center,
                                width: 60,
                                height: 60,
                                padding: EdgeInsets.all(15),

                                child: Icon(Icons.call, color: Colors.white, size: 24,)
                            )),
                        ),
                      ],
                    ),
                ),)

              ],
            )
        ),
      );
  }
  
}