import 'package:dating_app/utils/ColorUtils.dart';
import 'package:dating_app/utils/WidgetUtils.dart';
import 'package:flutter/material.dart';


class ProfilePage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProfilePageState();
  }
  
}
class ProfilePageState extends State<ProfilePage>
    with SingleTickerProviderStateMixin {
  bool _status = true;
  final FocusNode myFocusNode = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: ColorUtils.colorBg,
        body: new Container(
          color:  ColorUtils.colorBg,
          child: new ListView(
            children: <Widget>[
              Column(
                children: <Widget>[
                  new Container(

                    color: ColorUtils.colorBg,
                    child: new Column(
                      children: <Widget>[

                        Padding(
                          padding: EdgeInsets.only(top: 20.0),
                          child: new Stack(fit: StackFit.loose, children: <Widget>[
                            new Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Container(
                                    width: 140.0,
                                    height: 140.0,
                                    decoration: new BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: new DecorationImage(
                                        image: new ExactAssetImage(
                                            'assets/img/img.png'),
                                        fit: BoxFit.cover,
                                      ),
                                    )),
                              ],
                            ),
                            Padding(
                                padding: EdgeInsets.only(top: 90.0, right: 100.0),
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new CircleAvatar(
                                      backgroundColor: ColorUtils.colorButton,
                                      radius: 25.0,
                                      child: new Icon(
                                        Icons.camera_alt,
                                        color: Colors.white,
                                      ),
                                    )
                                  ],
                                )),
                          ]),
                        )
                      ],
                    ),
                  ),
                  new Container(

                    child: Padding(
                      padding: EdgeInsets.only(bottom: 10.0),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Text(
                                        'Parsonal Information',
                                        style: WidgetUtils.textStyleViewBold(ColorUtils.colorBg, 16 ),
                                      ),
                                    ],
                                  ),
                                ],
                              )),

                          viewSingup()
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ));
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    myFocusNode.dispose();
    super.dispose();
  }
  Widget viewSingup(){
    return Container(

      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [


          Container(

            margin: EdgeInsets.only(top: 15,left: 15,right: 15),
            child:     TextFormField(
                keyboardType: TextInputType.name,
                style: WidgetUtils.textStyleView(Colors.white),
                decoration:WidgetUtils.inputDecoration(Colors.white,Icons.person,Colors.white,"Name")
            ),
          ),
          SizedBox(height: 10,),
          Container(

            margin: EdgeInsets.only(top: 15,left: 15,right: 15),
            child:  TextFormField(
              style: WidgetUtils.textStyleView(Colors.white),
              decoration:WidgetUtils.inputDecoration(Colors.white,Icons.person,Colors.white,"Phone number"),
              keyboardType: TextInputType.phone,

            ),
          ),
          SizedBox(height: 10,),
          Container(
            margin: EdgeInsets.only(top: 15,left: 15,right: 15),
            child:     TextFormField(
              style: WidgetUtils.textStyleView(Colors.white),
              decoration:WidgetUtils.inputDecoration(Colors.white,Icons.person,Colors.white,"Email"),
              keyboardType: TextInputType.emailAddress,

            ),
          ),
          SizedBox(height: 10,),
          Container(

            margin: EdgeInsets.only(top: 15,left: 15,right: 15),
            child:     TextFormField(
              obscureText: true,
              keyboardType: TextInputType.visiblePassword,
              style: WidgetUtils.textStyleView(Colors.white),
              decoration:WidgetUtils.inputDecoration(Colors.white,Icons.person,Colors.white,"Password"),

            ),
          ),


          SizedBox(height: 35,),
          TextButton(onPressed: (){

          }, child:Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              elevation: 0,
              margin: EdgeInsets.only(left: 50,right: 50),
              color: ColorUtils.colorBg,
              child:  Container(
                alignment: Alignment.center,

                padding: EdgeInsets.all(15),

                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: ColorUtils.colorButton
                ),
                child: Text("Update",style: WidgetUtils.textStyleView1(Colors.white, 14),),
              )),
          ),

        ],
      ),
    );
  }
}