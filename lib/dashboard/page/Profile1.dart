import 'package:dating_app/dashboard/chat/colors.dart';
import 'package:dating_app/setting/SettingScreen.dart';
import 'package:dating_app/utils/ColorUtils.dart';
import 'package:dating_app/utils/WidgetUtils.dart';
import 'package:flutter/material.dart';


import 'likes_json.dart';

class Profile1 extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return Profile1State();
  }
  
}
class Profile1State extends State<Profile1>{
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(

          children: [

            topView(context)
        ],
      ),
    );
  }

  Widget topView(BuildContext context){
    var size = MediaQuery.of(context).size;
    return Container(
      child: Column(
        children: [
          Center(child:
          Container(
            alignment: Alignment.center,
            width: 150,
            height: 150,
            decoration: BoxDecoration(
                border:Border.all(color: ColorUtils.colorBflight,width: 10) ,
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: AssetImage("assets/girls/img_1.jpeg"),
                    fit: BoxFit.fill)),
          ),),
          SizedBox(height: 20,),
          Center(
            child: Text(
              'Neha Khan',
              style: WidgetUtils.textStyleViewBold(Colors.white, 16 ),
            ),
          ),
          SizedBox(height: 20,),
          Divider(
            height: 2,
            thickness: 0.1,
            color: Colors.grey.shade500,
          ),
          SizedBox(height: 10,),
          Container(
        margin: EdgeInsets.only(left: 10,right: 10),
            alignment: Alignment.topLeft,
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'My Photos',
                  style: WidgetUtils.textStyleViewBold(Colors.white, 16 ),
                ),

              ],
            )
          ),
          SizedBox(height: 10,),
          Container(
            margin: EdgeInsets.all(5),
            child:   GridView(
              padding: EdgeInsets.all(5),

              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisSpacing: 5,
                crossAxisCount: 3,
              ),
              physics: NeverScrollableScrollPhysics(),

              shrinkWrap: true,
              /* gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                   crossAxisCount: 3,
                 ),*/
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child:    Image.asset("assets/girls/img_1.jpeg",width: 100,height: 100,fit: BoxFit.cover,),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child:    Image.asset("assets/girls/img_2.jpeg",width: 100,height: 100,fit: BoxFit.cover,),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child:    Image.asset("assets/girls/img_3.jpeg",width: 100,height: 100,fit: BoxFit.cover,),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child:    Image.asset("assets/girls/img_4.jpeg",width: 100,height: 100,fit: BoxFit.cover,),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child:    Image.asset("assets/girls/img_5.jpeg",width: 100,height: 100,fit: BoxFit.cover,),
                ),

                ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child:    Image.asset("assets/girls/img_6.jpeg",width: 100,height: 100,fit: BoxFit.cover,),
                ),


              ],
            ),
          ),
          SizedBox(height: 10,),

          Container(
            margin: EdgeInsets.only(left: 0),
            padding: EdgeInsets.all(1),
            child: ListTile(
              leading:   Icon(Icons.person,color: Colors.grey,),
              trailing:  Icon(Icons.arrow_forward_ios_sharp,color: Colors.grey,size: 20,),
              title: Text(
                'Account Details', style: WidgetUtils.textStyleView1(Colors.white, 14 ),),
            )
          ),
          Divider(
            height: 2,
            thickness: 0.1,
            color: Colors.grey.shade500,
          ),
          Container(
              margin: EdgeInsets.only(left: 0),
              padding: EdgeInsets.all(1),
              child: ListTile(
                onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>SettingScreen()));
                },
                leading:   Icon(Icons.settings,color: Colors.grey,),
                trailing:  Icon(Icons.arrow_forward_ios_sharp,color: Colors.grey,size: 20),
                title: Text(
                  'Settings', style: WidgetUtils.textStyleView1(Colors.white, 14 ),),
              )
          ),
          Divider(
            height: 2,
            thickness: 0.1,
            color: Colors.grey.shade500,
          ),
          Container(
              margin: EdgeInsets.only(left: 0),
              padding: EdgeInsets.all(1),
              child: ListTile(
                leading:   Icon(Icons.phone_android,color: Colors.grey,),
                trailing:  Icon(Icons.arrow_forward_ios_sharp,color: Colors.grey,size: 20),
                title: Text(
                  'Contact Us', style: WidgetUtils.textStyleView1(Colors.white, 14 ),),
              )
          ),
          Divider(
            height: 2,
            thickness: 0.1,
            color: Colors.grey.shade500,
          ),
          Container(
              margin: EdgeInsets.only(left: 0),
              padding: EdgeInsets.all(1),
              child: ListTile(
                leading:  Image.asset("assets/img/vip.png",width: 24,height: 24,),
                trailing:  Icon(Icons.arrow_forward_ios_sharp,color: Colors.grey,size: 20),
                title: Text(
                  'Upgrade Account', style: WidgetUtils.textStyleView1(Colors.white, 14 ),),
              )
          ),
          Divider(
            height: 2,
            thickness: 0.1,
            color: Colors.grey.shade500,
          ),
          SizedBox(height: 20,),
          TextButton(onPressed: (){
            // Navigator.push(context, MaterialPageRoute(builder: (context)=>Dashboard()));
          }, child:Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              elevation: 0,
              margin: EdgeInsets.only(left: 50,right: 50),
              color: ColorUtils.colorBflight,
              child:  Container(
                alignment: Alignment.center,

                padding: EdgeInsets.all(15),

                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: ColorUtils.colorBflight
                ),
                child: Text("Logout",style: WidgetUtils.textStyleViewBold(Colors.white, 14)),
              )),
          ),
        ],
      ),
    );
  }
  
}