import 'package:blurrycontainer/blurrycontainer.dart';
import 'package:countdown_flutter/countdown_flutter.dart';
import 'package:dating_app/utils/ColorUtils.dart';
import 'package:dating_app/utils/WidgetUtils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class VoiceCallScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return VoiceCallScreenState();
  }
  
}
class VoiceCallScreenState extends State<VoiceCallScreen>{
  @override
  Widget build(BuildContext context) {

    return Scaffold(

      body: Container(
          decoration: BoxDecoration(

              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage("assets/girls/img_1.jpeg")
              )
          ),
          child:   Stack(
            children: [
              BlurryContainer(
                bgColor: ColorUtils.colorBg,
                width: double.infinity,
                height: double.infinity,
                child:Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child:
                        Container(
                          alignment: Alignment.center,
                          width: 150,
                          height: 150,
                          decoration: BoxDecoration(
                              border:Border.all(color: ColorUtils.colorBflight,width: 1) ,
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: AssetImage("assets/girls/img_1.jpeg"),
                                  fit: BoxFit.cover)),
                        ),),
                        SizedBox(height: 10,),
                        Center(
                          child: Text(
                            'Neha Khan',
                            style: WidgetUtils.textStyleViewBold(Colors.white, 16 ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.zero,
                          child: CountdownFormatted(
                            duration: Duration(hours: 1),
                            builder: (BuildContext ctx, String remaining) {
                              return Text(remaining,style:WidgetUtils.textStyleView1(Colors.white, 14),); // 01:00:00
                            },
                          ),
                        ),
                      ],
                    ),
                ),

              ),



              Align(
                alignment: Alignment.topLeft,

                child:    InkWell(
                  onTap: (){
                    Navigator.pop(context,null);
                  },
                    child: Container(
                        margin: EdgeInsets.only(top: 15),
                        alignment: Alignment.center,
                        width: 60,
                        height: 60,
                        padding: EdgeInsets.all(15),

                        child: Icon(Icons.arrow_back_ios_outlined, color: Colors.white, size: 24,)
                    )
                ),
              ),
              Align(

                alignment: Alignment.bottomCenter,
                child: Container(

                  margin: EdgeInsets.only(bottom: 20),
                  child:  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [

                      TextButton(onPressed: () {
                      }, child: Card(

                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(80),
                          ),
                          elevation: 5,
                          color: ColorUtils.colorBflight,
                          child: Container(
                              alignment: Alignment.center,
                              width: 60,
                              height: 60,
                              padding: EdgeInsets.all(15),
                              child: Icon(
                                Icons.volume_down, color: Colors.white, size: 24,)
                          )),
                      ),
                      TextButton(onPressed: () {
                      }, child: Card(

                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(80),
                          ),
                          elevation: 5,

                          color: ColorUtils.colorBflight,
                          child: Container(
                              alignment: Alignment.center,
                              width: 60,
                              height: 60,
                              padding: EdgeInsets.all(15),

                              child: Icon(Icons.mic, color: Colors.white, size: 24,)
                          )),
                      ),
                      TextButton(onPressed: () {


                      }, child: Card(

                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(80),
                          ),
                          elevation: 5,
                          color: ColorUtils.colorRedNew,
                          child: Container(
                              alignment: Alignment.center,
                              width: 60,
                              height: 60,
                              padding: EdgeInsets.all(15),

                              child: Icon(Icons.call, color: Colors.white, size: 24,)
                          )),
                      ),
                      TextButton(onPressed: () {


                      }, child: Card(

                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(80),
                          ),
                          elevation: 5,
                          color: Colors.green,
                          child: Container(
                              alignment: Alignment.center,
                              width: 60,
                              height: 60,
                              padding: EdgeInsets.all(15),

                              child: Icon(Icons.call, color: Colors.white, size: 24,)
                          )),
                      ),
                    ],
                  ),
                ),)

            ],
          )
      ),
    );
  }

}