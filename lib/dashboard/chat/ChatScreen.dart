import 'dart:async';

import 'package:circular_reveal_animation/circular_reveal_animation.dart';
import 'package:dating_app/dashboard/video_call/VideoCallScreen.dart';
import 'package:dating_app/dashboard/voice_call/VoiceCallScreen.dart';
import 'package:dating_app/utils/ColorUtils.dart';
import 'package:dating_app/utils/WidgetUtils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'ChatMessage.dart';

class ChatScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ChatScreenState();
  }
  
}
class ChatScreenState extends State<ChatScreen>  with SingleTickerProviderStateMixin {
  late AnimationController animationController;
  late Animation<double> animation;
  ScrollController _controller = ScrollController();
  TextEditingController nameController = TextEditingController();
  List<ChatMessage> messages = [
    ChatMessage(messageContent: "Hello, Will", messageType: "receiver"),
    ChatMessage(messageContent: "How have you been?", messageType: "receiver"),
    ChatMessage(messageContent: "Hey Kriss, I am doing fine dude. wbu?", messageType: "sender"),
    ChatMessage(messageContent: "ehhhh, doing OK.", messageType: "receiver"),
    ChatMessage(messageContent: "Is there any thing wrong?", messageType: "sender"),
    ChatMessage(messageContent: "Hello, Will", messageType: "receiver"),
    ChatMessage(messageContent: "How have you been?", messageType: "receiver"),
    ChatMessage(messageContent: "Hey Kriss, I am doing fine dude. wbu?", messageType: "sender"),
    ChatMessage(messageContent: "ehhhh, doing OK.", messageType: "receiver"),
    ChatMessage(messageContent: "Is there any thing wrong?", messageType: "sender"),
    ChatMessage(messageContent: "Hello, Will", messageType: "receiver"),
    ChatMessage(messageContent: "How have you been?", messageType: "receiver"),
    ChatMessage(messageContent: "Hey Kriss, I am doing fine dude. wbu?", messageType: "sender"),
    ChatMessage(messageContent: "ehhhh, doing OK.", messageType: "receiver"),
    ChatMessage(messageContent: "Is there any thing wrong?", messageType: "sender"),
    ChatMessage(messageContent: "Hello, Will", messageType: "receiver"),
    ChatMessage(messageContent: "How have you been?", messageType: "receiver"),
    ChatMessage(messageContent: "Hey Kriss, I am doing fine dude. wbu?", messageType: "sender"),
    ChatMessage(messageContent: "ehhhh, doing OK.", messageType: "receiver"),
    ChatMessage(messageContent: "Is there any thing wrong?", messageType: "sender"),
    ChatMessage(messageContent: "Hello, Will", messageType: "receiver"),
    ChatMessage(messageContent: "How have you been?", messageType: "receiver"),
    ChatMessage(messageContent: "Hey Kriss, I am doing fine dude. wbu?", messageType: "sender"),
    ChatMessage(messageContent: "ehhhh, doing OK.", messageType: "receiver"),


  ];
  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 1000),
    );
    animation = CurvedAnimation(
      parent: animationController,
      curve: Curves.easeIn,
    );
  }

  final itemKey=GlobalKey();

  Future scrollToItem() async{
    //final context=itemKey.currentContext;
    await Scrollable.ensureVisible(context);
  }
  void addItemToList(){

    setState(() {
      messages.insert(messages.length,   ChatMessage(messageContent: nameController.text, messageType: "sender"),);
      _controller.animateTo(_controller.position.maxScrollExtent, duration: Duration(milliseconds: 300), curve: Curves.easeInOut);
      });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      backgroundColor: ColorUtils.colorBg,
     // bottomNavigationBar:

        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          backgroundColor: ColorUtils.colorBg,
          actions: [
            IconButton(
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>VoiceCallScreen()));
              },
              icon: Icon(Icons.call_end,color: Colors.white,),
            ),

            SizedBox(width: 10,),
            IconButton(
              onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>VideoCallScreen()));
              },
              icon: Icon(Icons.video_call_sharp,color: Colors.white,),
            ),
            IconButton(
              onPressed: (){

              },
              icon: Icon(Icons.more_vert,color: Colors.white,),
            ),

          ],
          flexibleSpace: SafeArea(
            child: Container(
              padding: EdgeInsets.only(right: 16),
              child: Row(
                children: <Widget>[
                  IconButton(
                    onPressed: (){
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.arrow_back,color: Colors.white,),
                  ),
                  SizedBox(width: 2,),
                /*  CircleAvatar(
                    backgroundImage: NetworkImage("<https://randomuser.me/api/portraits/men/5.jpg>"),
                    maxRadius: 20,
                  ),*/
                  SizedBox(width: 12,),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("Kriss Benwat",style: WidgetUtils.textStyleView1(Colors.white, 14),),
                        SizedBox(height: 6,),
                        Text("Online",style: WidgetUtils.textStyleView1(Colors.white, 12),),
                      ],
                    ),
                  ),

                ],
              ),
            ),
          ),
        ),
        body:  Column(
          children: [
            Expanded(child:  ListView.builder(
              controller: _controller,
              itemCount: messages.length,
              shrinkWrap: true,

              padding: EdgeInsets.only(top: 10,bottom: 10),

              itemBuilder: (context, index){
                return Container(
                  padding: EdgeInsets.only(left: 14,right: 14,top: 10,bottom: 10),
                  child: Align(
                    alignment: (messages[index].messageType == "receiver"?Alignment.topLeft:Alignment.topRight),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: (messages[index].messageType  == "receiver"?ColorUtils.colorFB:ColorUtils.colorBflight),
                      ),
                      padding: EdgeInsets.all(16),
                      child: Text(messages[index].messageContent, style: WidgetUtils.textStyleView1(Colors.white, 14),),
                    ),
                  ),
                );
              },
            ),),
            Align(
              alignment: Alignment.bottomCenter,
              child:Container(
                color: ColorUtils.colorBflight,
                padding: EdgeInsets.only(left: 10,bottom: 10,top: 10),
                height: 60,
                width: double.infinity,

                child: Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: (){

                         showRevealDialog(context);
                      },
                      child: Container(
                        height: 30,
                        width: 30,
                        decoration: BoxDecoration(
                          color: ColorUtils.colorBg,
                          borderRadius: BorderRadius.circular(30),
                        ),
                        child: Icon(Icons.add, color: Colors.white, size: 20, ),
                      ),
                    ),
                    SizedBox(width: 15,),
                    Expanded(
                      child: TextField(
                          onTap: (){
                            setState(() {
                              _controller.animateTo(_controller.position.maxScrollExtent, duration: Duration(milliseconds: 300), curve: Curves.easeInOut);
                            });

                          },
                        controller: nameController,
                        style: WidgetUtils.textStyleView1(Colors.white, 14),
                        decoration: InputDecoration(
                            hintText: "Write message...",
                            hintStyle: TextStyle(color: Colors.white),
                            border: InputBorder.none
                        ),
                      ),
                    ),
                    SizedBox(width: 15,),
                    FloatingActionButton(
                      onPressed: (){
                        addItemToList();
                        nameController.text="";
                        FocusScopeNode currentFocus = FocusScope.of(context);

                        if (!currentFocus.hasPrimaryFocus) {
                          currentFocus.unfocus();
                        }
                      },
          
                      child: Icon(Icons.send,color: Colors.white,size: 18,),
                      backgroundColor: ColorUtils.colorBg,
                      elevation: 0,
                    ),
                  ],

                ),
              ), //may i come in  i am going to pray
            )
          ],

        )
    );
  }

  Future<void> showRevealDialog(BuildContext context) async {
    showGeneralDialog(

      anchorPoint: const  Offset(1.0, 1.0),
      barrierLabel: "Label",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 700),
      context: context,
      pageBuilder: (context, anim1, anim2) {
        return Align(
          alignment: Alignment.bottomCenter ,
          child: Container(
            alignment: Alignment.center,

            height: 200,
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [

                      FloatingActionButton(
                        backgroundColor:ColorUtils.colorBflight ,

                        onPressed: (){},
                        child:  Image.asset("assets/images/ic_document.png"),
                      ),
                      FloatingActionButton(onPressed: (){},
                        backgroundColor:ColorUtils.colorBflight ,
                        child: const Icon(Icons.navigation),
                      ),
                      FloatingActionButton(onPressed: (){},
                        backgroundColor:ColorUtils.colorBflight ,
                        child: const Icon(Icons.navigation),
                      ),

                    ],
                  ),
                  SizedBox(height: 30,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [

                      FloatingActionButton(onPressed: (){},
                        backgroundColor:ColorUtils.colorBflight ,

                child: const Icon(Icons.navigation),
                      ),
                     /* Flexible(
                        child:   ClipOval(
                            child:Container(
                              alignment: Alignment.center,
                              width: 60,
                              height: 60,
                              color: ColorUtils.colorBflight,
                              child: Image.asset("assets/images/ic_gift.png",fit:BoxFit.fill,height: 24,width: 24,),
                            )
                        ),
                      ),*/
                      FloatingActionButton(onPressed: (){},
                        backgroundColor:ColorUtils.colorBflight ,
                        child: const Icon(Icons.navigation),
                      ),
                      FloatingActionButton(onPressed: (){},
                        backgroundColor:ColorUtils.colorBflight ,
                        child: const Icon(Icons.navigation),
                      ),

                    ],
                  ),

                ],
              )
            ),
            margin: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 80  ),
            decoration: BoxDecoration(
              color: ColorUtils.colorBg,
              borderRadius: BorderRadius.circular(20),
            ),
          ),
        );
      },
      transitionBuilder: (context, anim1, anim2, child) {
        return CircularRevealAnimation(
          child: child,
          animation: anim1,
          centerAlignment: Alignment.bottomCenter,
        );
      },
    );
  }
}