
import 'package:buttons_tabbar/buttons_tabbar.dart';
import 'package:countdown_flutter/countdown_flutter.dart';
import 'package:dating_app/utils/AppUtils.dart';

import 'package:dating_app/utils/ColorUtils.dart';
import 'package:dating_app/utils/WidgetUtils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';

import 'package:flutter_tindercard/flutter_tindercard.dart';
import 'package:getwidget/components/carousel/gf_carousel.dart';

import 'colors.dart';
import 'explore_json.dart';

class ExplorePage extends StatefulWidget {
  @override
  _ExplorePageState createState() => _ExplorePageState();
}

class _ExplorePageState extends State<ExplorePage>
    with TickerProviderStateMixin {
  late CardController controller;
  List<bool> _isSelected = [false, true, false];
  int _currentIndex = 0;
  List itemsTemp = [];
  int itemLength = 0;
  bool visibleLike = false;
  bool visibleUnlike = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      itemsTemp = explore_json;
      itemLength = explore_json.length;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.colorBg,
      body: getBody(),
      bottomSheet: getBottomSheet(),
    );
  }

  Widget getBody() {
    var size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.only(bottom: 120),
      child: Container(
        height: size.height,
        child: TinderSwapCard(
          totalNum: itemLength,
          maxWidth: MediaQuery.of(context).size.width,
          maxHeight: MediaQuery.of(context).size.height * 0.75,
          minWidth: MediaQuery.of(context).size.width * 0.75,
          minHeight: MediaQuery.of(context).size.height * 0.6,
          cardBuilder: (context, index) => Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                      color: grey.withOpacity(0.3),
                      blurRadius: 5,
                      spreadRadius: 2),
                ]),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Stack(
                children: [
                  Container(
                    width: size.width,
                    height: size.height,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(itemsTemp[index]['img']),
                          fit: BoxFit.cover),
                    ),
                  ),

                Align(
                  alignment: Alignment.topRight,
                  child:   Visibility(
                  visible: visibleLike,
                    child:   Container(
                      margin: EdgeInsets.all(20),
                      alignment: Alignment.topRight,
                      child: Text("Like".toUpperCase(),style: TextStyle(color:Colors.green,fontWeight: FontWeight.bold,fontSize: 35,fontFamily: AppUtils.fontTelma,shadows: [
                        Shadow(
                          blurRadius: 10.0,
                          color: Colors.green,
                          offset: Offset(5.0,5.0)
                        )
                      ]),),
                    ),),
                ),
                  Align(
                    alignment: Alignment.topLeft,
                    child:   Visibility(
                      visible: visibleUnlike,
                      child:   Container(
                        margin: EdgeInsets.all(20),
                        alignment: Alignment.topLeft,
                        child:Text("UnLike".toUpperCase(),style: TextStyle(color:Colors.red,  fontWeight: FontWeight.bold,fontSize: 35,fontFamily: AppUtils.fontTelma,shadows: [
                          Shadow(
                              blurRadius: 10.0,
                              color: Colors.red,
                              offset: Offset(5.0,5.0)
                          )
                        ]),),
                      ),),
                  ),

                  Container(
                    width: size.width,
                    height: size.height,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [
                          black.withOpacity(0.25),
                          black.withOpacity(0),
                        ],
                            end: Alignment.topCenter,
                            begin: Alignment.bottomCenter)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(0),
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(bottom: 20,left: 10),
                                width: size.width * 0.72,
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          itemsTemp[index]['name'],
                                          style: TextStyle(
                                              color: white,
                                              fontSize: 24,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Text(
                                          itemsTemp[index]['age'],
                                          style: TextStyle(
                                            color: white,
                                            fontSize: 22,
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          width: 10,
                                          height: 10,
                                          decoration: BoxDecoration(
                                              color: green,
                                              shape: BoxShape.circle),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Text(
                                          "Recently Active",
                                          style: TextStyle(
                                            color: white,
                                            fontSize: 16,
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    Row(
                                      children: List.generate(
                                          itemsTemp[index]['likes'].length,
                                          (indexLikes) {
                                        if (indexLikes == 0) {
                                          return Padding(
                                            padding:
                                                const EdgeInsets.only(right: 8),
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  border: Border.all(
                                                      color: white, width: 2),
                                                  borderRadius:
                                                      BorderRadius.circular(30),
                                                  color:
                                                      white.withOpacity(0.4)),
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 3,
                                                    bottom: 3,
                                                    left: 10,
                                                    right: 10),
                                                child: Text(
                                                  itemsTemp[index]['likes']
                                                      [indexLikes],
                                                  style:
                                                      TextStyle(color: white),
                                                ),
                                              ),
                                            ),
                                          );
                                        }
                                        return Padding(
                                          padding:
                                              const EdgeInsets.only(right: 8),
                                          child: Container(
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(30),
                                                color: white.withOpacity(0.2)),
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 3,
                                                  bottom: 3,
                                                  left: 10,
                                                  right: 10),
                                              child: Text(
                                                itemsTemp[index]['likes']
                                                    [indexLikes],
                                                style: TextStyle(color: white),
                                              ),
                                            ),
                                          ),
                                        );
                                      }),
                                    )
                                  ],
                                ),
                              ),


                              Expanded(
                                child: Container(
                                  width: size.width * 0.2,
                                  child: InkWell(
                                    onTap: (){
                                      showSheet(context);
                                    },
                                    child: Icon(
                                      Icons.info,
                                      color: white,
                                      size: 28,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          cardController: controller = CardController(),
          swipeUpdateCallback: (DragUpdateDetails details, Alignment align) {
            /// Get swiping card's alignment
              if (align.x < 0) {
                print("=====================lit");
              setState(() {

                  visibleLike=true;
                  visibleUnlike=false;
                });

              //Card is LEFT swiping
            } else if (align.x > 0) {
                print("=====================Right");
                setState(() {
                  visibleLike=false;
                  visibleUnlike=true;
                });
              //Card is RIGHT swiping
            }
            // print(itemsTemp.length);
          },
          swipeCompleteCallback: (CardSwipeOrientation orientation, int index) {
            /// Get orientation & index of swiped card!
            if (index == (itemsTemp.length - 1)) {
              setState(() {
                visibleLike=false;

                itemLength = itemsTemp.length - 1;
              });
            }
            else{
              setState(() {
                visibleLike=false;
                visibleUnlike=false;
              });
            }
          },
        ),
      ),
    );
  }

  Widget getBottomSheet() {
    var size = MediaQuery.of(context).size;

    return   Container(
      color: ColorUtils.colorBg,
      width: size.width,
      padding: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          TextButton(onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  contentPadding: EdgeInsets.zero,
                  backgroundColor:ColorUtils.colorBg ,
                  elevation: 0,
                  insetPadding: EdgeInsets.symmetric(horizontal: 0),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),

                  content: Container(
                    height: MediaQuery.of(context).size.height - 300,

                    width: MediaQuery.of(context).size.width - 70,
                    padding: EdgeInsets.zero  ,
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.zero,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                width: double.infinity,
                                padding: EdgeInsets.zero,

                                height:250,
                                color: ColorUtils.colorBflight,
                                child: Column(

                                  children: [
                                    SizedBox(height: 10,),
                                    Container(
                                      alignment: Alignment.center,
                                      padding: EdgeInsets.zero,
                                      child: Text("Get Plus+",style:WidgetUtils.textStyleViewBold(Colors.white, 20),),
                                    ),
                                    SizedBox(height: 20,),
                                    Container(
                                      width: 70,
                                      height: 70,
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: DecorationImage(
                                              image: AssetImage("assets/img/img.png"),
                                              fit: BoxFit.cover)),
                                    ),
                                    SizedBox(height: 20,),
                                    Container(
                                      alignment: Alignment.center,
                                      padding: EdgeInsets.zero,
                                      child: Text("Unlimited Likes",style:WidgetUtils.textStyleViewBold(Colors.white, 16),),
                                    ),
                                    Container(
                                      alignment: Alignment.center,
                                      padding: EdgeInsets.zero,
                                      child: CountdownFormatted(
                                        duration: Duration(hours: 1),
                                        builder: (BuildContext ctx, String remaining) {
                                          return Text(remaining,style:WidgetUtils.textStyleViewBold(Colors.white, 20),); // 01:00:00
                                        },
                                      ),
                                    ),
                                    SizedBox(height: 20,),
                                  ],
                                ),
                              ),
                                DefaultTabController(
                                  length: 3,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                        ButtonsTabBar(
                                          physics: NeverScrollableScrollPhysics(),
                                          buttonMargin: EdgeInsets.zero,
                                        elevation: 0,
                                        radius: 0,
                                        contentPadding: EdgeInsets.zero,
                                        height: 150 ,
                                        backgroundColor:ColorUtils.colorBg  ,
                                        unselectedBackgroundColor: ColorUtils.colorBflight,
                                        unselectedLabelStyle: TextStyle(color: Colors.black),
                                        labelStyle:
                                        TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                                        tabs: [
                                          Tab(

                                            icon: Container(

                                           decoration: BoxDecoration(
                                         shape: BoxShape.rectangle,
                                          border: Border.all(
                                        color: ColorUtils.colorRedNew, width: 1)),

                                              padding: EdgeInsets.only(left: 16,right: 16,top: 10,bottom: 10),
                                              alignment: Alignment.center,

                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    '12,',
                                                    style: WidgetUtils.textStyleView1(Colors.white, 16),
                                                  ),
                                                  SizedBox(height: 10,),
                                                  Text(
                                                    'months',
                                                    style: WidgetUtils.textStyleView1(Colors.white, 14),
                                                  ),
                                                  SizedBox(height: 10,),
                                                  Text(
                                                    '\$191.98/mo,',
                                                    style: WidgetUtils.textStyleView1(Colors.white, 16),
                                                  ),
                                                  SizedBox(height: 10,),
                                                  Text(
                                                    'Save 50%',
                                                    style: WidgetUtils.textStyleView1(Colors.white, 14),
                                                  ),
                                                ],

                                              ),
                                            ),
                                            text:"",
                                          ),
                                          Tab(
                                            icon: Container(

                                              decoration: BoxDecoration(
                                                  shape: BoxShape.rectangle,
                                                  border: Border.all(
                                                      color: ColorUtils.colorRedNew, width: 1)),
                                              padding: EdgeInsets.only(left: 16,right: 16,top: 10,bottom: 10),
                                              alignment: Alignment.center,

                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    '12,',
                                                    style: WidgetUtils.textStyleView1(Colors.white, 16),
                                                  ),
                                                  SizedBox(height: 10,),
                                                  Text(
                                                    'months',
                                                    style: WidgetUtils.textStyleView1(Colors.white, 14),
                                                  ),
                                                  SizedBox(height: 10,),
                                                  Text(
                                                    '\$191.98/mo,',
                                                    style: WidgetUtils.textStyleView1(Colors.white, 16),
                                                  ),
                                                  SizedBox(height: 10,),
                                                  Text(
                                                    'Save 50%',
                                                    style: WidgetUtils.textStyleView1(Colors.white, 14),
                                                  ),
                                                ],

                                              ),
                                            ),
                                            text:"",
                                          ),
                                          Tab(
                                            icon: Container(
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.rectangle,
                                                  border: Border.all(
                                                      color: ColorUtils.colorRedNew, width: 1)),
                                              padding: EdgeInsets.only(left: 18,right: 18,top: 10,bottom: 10),
                                              alignment: Alignment.center,

                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    '12,',
                                                    style: WidgetUtils.textStyleView1(Colors.white, 16),
                                                  ),
                                                  SizedBox(height: 10,),
                                                  Text(
                                                    'months',
                                                    style: WidgetUtils.textStyleView1(Colors.white, 14),
                                                  ),
                                                  SizedBox(height: 10,),
                                                  Text(
                                                    '\$191.98/mo,',
                                                    style: WidgetUtils.textStyleView1(Colors.white, 16),
                                                  ),
                                                  SizedBox(height: 10,),
                                                  Text(
                                                    'Save 50%',
                                                    style: WidgetUtils.textStyleView1(Colors.white, 14),
                                                  ),
                                                ],

                                              ),
                                            ),
                                            text:"",
                                          ),
                                         /* Tab(
                                            icon: Icon(Icons.directions_car),
                                            text: "car",
                                          ),
                                          Tab(
                                            icon: Icon(Icons.directions_transit),
                                            text: "transit",
                                          ),
                                          Tab(icon: Icon(Icons.directions_bike)),*/

                                        ],

                                      )
                                    ],
                                  ),
                                  
                                ),
                              SizedBox(height: 40,),
                              TextButton(onPressed: (){

                              }, child:Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  elevation: 0,
                                  margin: EdgeInsets.only(left: 50,right: 50),
                                  color: ColorUtils.colorRedNew,
                                  child:  Container(
                                    alignment: Alignment.center,

                                    padding: EdgeInsets.all(15),

                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        color: ColorUtils.colorRedNew
                                    ),
                                    child: Text("Update",style: WidgetUtils.textStyleView1(Colors.white, 14),),
                                  )),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                );
              },
            );
            //controller.triggerSwipeLeft();
          }, child: Card(

              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(80),
              ),
              elevation: 5,
              margin: EdgeInsets.only(left: 0, right: 0),
              color: ColorUtils.colorBflight,
              child: Container(
                  alignment: Alignment.center,
                  width: 55,
                  height: 55,
                  padding: EdgeInsets.all(10),

                  child: Icon(Icons.star, color: Colors.amber, size: 24,)
              )),
          ),
          TextButton(onPressed: () {
              setState(() {
                visibleLike=true;

              });
            controller.triggerLeft();

        //    _cardController.triggerSwipeRight();
          }, child: Card(

              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(80),
              ),
              elevation: 5,
              margin: EdgeInsets.only(left: 0, right: 0),
              color: ColorUtils.colorBflight,
              child: Container(
                  alignment: Alignment.center,
                  width: 55,
                  height: 55,
                  padding: EdgeInsets.all(15),
                  child: Icon(
                    Icons.favorite, color: Colors.pink, size: 24,)
              )),
          ),
          TextButton(onPressed: () {
            controller.triggerRight();
            visibleUnlike=true;
            //_cardController.triggerSwipeDown();
            // Navigator.push(context, MaterialPageRoute(builder: (context)=>Dashboard()));
          }, child: Card(

              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(80),
              ),
              elevation: 5,

              color: ColorUtils.colorBflight,
              child: Container(
                  alignment: Alignment.center,
                  width: 55,
                  height: 55,
                  padding: EdgeInsets.all(15),

                  child: Icon(Icons.close, color: Colors.white, size: 24,)
              )),
          ),
          TextButton(onPressed: () {

                  showSheet(context);
         //   controller.triggerDown();
          //  _cardController.triggerSwipeUp();
            // Navigator.push(context, MaterialPageRoute(builder: (context)=>Dashboard()));
          }, child: Card(

              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(80),
              ),
              elevation: 5,
              margin: EdgeInsets.only(left: 0, right: 0),
              color: ColorUtils.colorBflight,
              child: Container(
                  alignment: Alignment.center,
                  width: 55,
                  height: 55,
                  padding: EdgeInsets.all(15),

                  child: Image.asset(
                    "assets/img/flash.png", color: Colors.green,
                    width: 30,
                    height: 30,)
              )),
          ),
        /*  TextButton(onPressed: () {
            // Navigator.push(context, MaterialPageRoute(builder: (context)=>Dashboard()));
          }, child: Card(

              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(80),
              ),
              elevation: 5,
              margin: EdgeInsets.only(left: 0, right: 0),
              color: ColorUtils.colorBflight,
              child: Container(
                  alignment: Alignment.center,
                  width: 55,
                  height: 55,
                  padding: EdgeInsets.all(15),

                  child: Icon(Icons.info, color: Colors.white, size: 24,)
              )),
          ),*/
        ],
      ),
    );

  }

  Widget getBottomSheet1() {
    var size = MediaQuery.of(context).size;

    return   Container(
      color: ColorUtils.colorBg,
      width: size.width,
      padding: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [

          TextButton(onPressed: () {

            //    _cardController.triggerSwipeRight();
          }, child: Card(

              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(80),
              ),
              elevation: 5,
              margin: EdgeInsets.only(left: 0, right: 0),
              color: ColorUtils.colorBflight,
              child: Container(
                  alignment: Alignment.center,
                  width: 60,
                  height: 60,
                  padding: EdgeInsets.all(15),
                  child: Icon(
                    Icons.favorite, color: Colors.pink, size: 24,)
              )),
          ),
          TextButton(onPressed: () {
            //_cardController.triggerSwipeDown();
            // Navigator.push(context, MaterialPageRoute(builder: (context)=>Dashboard()));
          }, child: Card(

              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(80),
              ),
              elevation: 5,

              color: ColorUtils.colorBflight,
              child: Container(
                  alignment: Alignment.center,
                  width: 60,
                  height: 60,
                  padding: EdgeInsets.all(15),

                  child: Icon(Icons.close, color: Colors.white, size: 24,)
              )),
          ),
          TextButton(onPressed: () {

            //   controller.triggerDown();
            //  _cardController.triggerSwipeUp();
            // Navigator.push(context, MaterialPageRoute(builder: (context)=>Dashboard()));
          }, child: Card(

              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(80),
              ),
              elevation: 5,
              margin: EdgeInsets.only(left: 0, right: 0),
              color: ColorUtils.colorBflight,
              child: Container(
                  alignment: Alignment.center,
                  width: 60,
                  height: 60,
                  padding: EdgeInsets.all(15),

                  child: Image.asset(
                    "assets/img/flash.png", color: Colors.green,
                    width: 30,
                    height: 30,)
              )),
          ),
          /*  TextButton(onPressed: () {
            // Navigator.push(context, MaterialPageRoute(builder: (context)=>Dashboard()));
          }, child: Card(

              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(80),
              ),
              elevation: 5,
              margin: EdgeInsets.only(left: 0, right: 0),
              color: ColorUtils.colorBflight,
              child: Container(
                  alignment: Alignment.center,
                  width: 55,
                  height: 55,
                  padding: EdgeInsets.all(15),

                  child: Icon(Icons.info, color: Colors.white, size: 24,)
              )),
          ),*/
        ],
      ),
    );

  }
  void showSheet(BuildContext context) {
    var size = MediaQuery.of(context).size;
    //var _current=0;
    showModalBottomSheet(
      context: context,

      isScrollControlled: true, // set this to true
      builder: (_) {
          return DraggableScrollableSheet(

          expand: false,
          builder: (_, controller) {
            return Scaffold(
                bottomSheet: getBottomSheet1(),
                  backgroundColor: ColorUtils.colorBg,
                body: SingleChildScrollView(
                  controller: controller,
                  child: Container(

                    child:  Column(
                      children: [
                        Stack(
                          alignment: Alignment.topRight,
                          textDirection: TextDirection.rtl,
                          fit: StackFit.loose,
                          clipBehavior: Clip.hardEdge,
                          children: <Widget>[
                            new Container(
                              color: ColorUtils.colorRedNew,
                              child: new Column(
                                children: <Widget>[

                                  ImageSlideshow(
                                    width: size.width,

                                    height: MediaQuery.of(context).size.height * 0.30,
                                    initialPage: 0,
                                    indicatorColor: Colors.blue,
                                    indicatorBackgroundColor: Colors.red,
                                    children: [
                                      ClipRRect(
                                        child: Image.asset(
                                            "assets/girls/img_1.jpeg",
                                            fit: BoxFit.cover,
                                            height: 300,
                                            width:double.infinity
                                        ),
                                      ),
                                      ClipRRect(
                                        child: Image.asset(
                                            "assets/girls/img_1.jpeg",
                                            fit: BoxFit.cover,
                                            height: 300,
                                            width:double.infinity
                                        ),
                                      ),
                                      ClipRRect(
                                        child: Image.asset(
                                            "assets/girls/img_1.jpeg",
                                            fit: BoxFit.cover,
                                            height: 300,
                                            width:double.infinity
                                        ),
                                      )
                                    ],
                                    onPageChanged: (value) {
                                      print('Page changed: $value');
                                    },
                                    autoPlayInterval: 3000,
                                    isLoop: true,
                                  ),
                                ],
                              ),
                            ),
                            Positioned(
                                top: 230,right: 10,
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new CircleAvatar(
                                        backgroundColor: ColorUtils.colorRedNew,
                                        radius: 35.0,
                                        child: new Image.asset("assets/img/down_arrow.png",width: 24,height: 24,color: Colors.white,)
                                    )
                                  ],
                                )),


                          ],
                        ),
                        Container(
                          margin: EdgeInsets.all(5),
                          padding: EdgeInsets.all(2),
                          child: Column(
                          children: [
                            Row(
                              children: [
                                Text(
                                  'Neha Khan',
                                  style: WidgetUtils.textStyleViewBold(Colors.white, 18 ),
                                ),
                                Text(
                                  ' 30',
                                  style: WidgetUtils.textStyleView1(Colors.white, 16 ),
                                ),
                                SizedBox(width: 20,),
                                Image.asset("assets/img/gold_medal.png",width: 24,height: 24,)
                              ],
                            )
                          ],
                        ),
                        ),
                        SizedBox(height: 5,),
                        Container(
                          margin: EdgeInsets.only(left: 5),
                          padding: EdgeInsets.all(2),
                          child: Column(
                            children: [
                              Row(
                                children: [

                                  Icon(Icons.work,color: Colors.grey,),
                                  SizedBox(width: 10,),
                                  Text(
                                    'Android Developer',
                                    style: WidgetUtils.textStyleView1(Colors.white, 14 ),
                                  ),

                                ],
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 5,),
                        Container(
                          margin: EdgeInsets.only(left: 5),
                          padding: EdgeInsets.all(2),
                          child: Column(
                            children: [
                              Row(
                                children: [

                                  Icon(Icons.school,color: Colors.grey,),
                                  SizedBox(width: 10,),
                                  Text(
                                    'Gujarat University',
                                    style: WidgetUtils.textStyleView1(Colors.white, 14 ),
                                  ),

                                ],
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 5,),
                        Container(
                          margin: EdgeInsets.only(left: 5),
                          padding: EdgeInsets.all(2),
                          child: Column(
                            children: [
                              Row(
                                children: [

                                  Icon(Icons.location_on,color: Colors.grey,),
                                  SizedBox(width: 10,),
                                  Text(
                                    '5 km away',
                                    style: WidgetUtils.textStyleView1(Colors.white, 14 ),
                                  ),

                                ],
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 20,),
                        Divider(
                          color: Colors.grey,
                        ),
                        Container(
                          margin: EdgeInsets.all(5),
                          padding: EdgeInsets.all(2),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Text(
                                    'About me',
                                    style: WidgetUtils.textStyleViewBold(Colors.white, 18 ),
                                  ),

                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 5),
                          padding: EdgeInsets.all(2),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Filler text is text that shares some characteristics of a real written text, but is random or otherwise generated. It may be used to display a sample of fonts, generate text for testing, or to spoof an e-mail spam filter',
                               textAlign: TextAlign.left, style: WidgetUtils.textStyleView1(Colors.white, 14 ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 20,),
                        Divider(
                          color: Colors.grey,
                        ),
                        Container(
                          margin: EdgeInsets.all(5),
                          padding: EdgeInsets.all(2),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Text(
                                    'About me',
                                    style: WidgetUtils.textStyleViewBold(Colors.white, 18 ),
                                  ),

                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 5),
                          padding: EdgeInsets.all(2),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Filler text is text that shares some characteristics of a real written text, but is random or otherwise generated. It may be used to display a sample of fonts, generate text for testing, or to spoof an e-mail spam filter',
                                textAlign: TextAlign.left, style: WidgetUtils.textStyleView1(Colors.white, 14 ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 20,),
                        Divider(
                          color: Colors.grey,
                        ),
                        Container(
                          margin: EdgeInsets.all(5),
                          padding: EdgeInsets.all(2),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Text(
                                    'About me',
                                    style: WidgetUtils.textStyleViewBold(Colors.white, 18 ),
                                  ),

                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 5),
                          padding: EdgeInsets.all(2),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Filler text is text that shares some characteristics of a real written text, but is random or otherwise generated. It may be used to display a sample of fonts, generate text for testing, or to spoof an e-mail spam filter',
                                textAlign: TextAlign.left, style: WidgetUtils.textStyleView1(Colors.white, 14 ),
                              ),
                            ],
                          ),
                        ),

                      ],
                    )
                  ),
                )
            );
          },
        );
      },
    );
  }


}
