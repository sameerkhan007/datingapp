import 'package:dating_app/utils/ColorUtils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';

class ProfileApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(

              child: Container(
                width: double.infinity,

                child: Center(
                  child: Stack(
                    alignment: Alignment.topRight,
                    textDirection: TextDirection.rtl,
                    fit: StackFit.loose,

                    clipBehavior: Clip.hardEdge,
                    children: <Widget>[
                      new Container(
                        height: 200.0,
                        color: ColorUtils.colorRedNew,
                        child: new Column(
                          children: <Widget>[

                            ImageSlideshow(
                              width: double.infinity,

                              initialPage: 0,
                              indicatorColor: Colors.blue,
                              indicatorBackgroundColor: Colors.red,
                              children: [
                                ClipRRect(
                                  child: Image.asset(
                                      "assets/girls/img_1.jpeg",
                                      fit: BoxFit.cover,
                                      height: 200,
                                      width:double.infinity
                                  ),
                                ),
                                ClipRRect(
                                  child: Image.asset(
                                      "assets/girls/img_1.jpeg",
                                      fit: BoxFit.cover,
                                      height: 200,
                                      width:double.infinity
                                  ),
                                ),
                                ClipRRect(
                                  child: Image.asset(
                                      "assets/girls/img_1.jpeg",
                                      fit: BoxFit.cover,
                                      height: 200,
                                      width:double.infinity
                                  ),
                                )
                              ],
                              onPageChanged: (value) {
                                print('Page changed: $value');
                              },
                              autoPlayInterval: 3000,
                              isLoop: true,
                            ),
                          ],
                        ),
                      ),
                      Positioned(
                            top: 170,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new CircleAvatar(
                                backgroundColor: ColorUtils.colorBg,
                                radius: 35.0,
                                child: new Image.asset("assets/img/down_arrow.png",width: 24,height: 24,color: Colors.white,)
                              )
                            ],
                          )),


                    ],
                  ),
                ),
              )
          ),

        ],
      ),
    );
  }
}