import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:dating_app/ForgotPassword.dart';
import 'package:dating_app/Signup/SignupScreen.dart';
import 'package:dating_app/dashboard/chat/explore_page.dart';
import 'package:dating_app/dashboard/page/ChatPage.dart';

import 'package:dating_app/dashboard/page/MatchesPage.dart';
import 'package:dating_app/dashboard/page/Profile1.dart';
import 'package:dating_app/dashboard/page/ProfilePage.dart';

import 'package:dating_app/login/Login.dart';
import 'package:dating_app/utils/AppUtils.dart';
import 'package:dating_app/utils/ColorUtils.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DashboardState();
  }
  
}
class DashboardState extends State<Dashboard>  with TickerProviderStateMixin{

  String title="Home";
  List<Widget> welcomeImages1 = [
    ExplorePage(),
    MatchesPage(),
    ChatPage(),
    Profile1(),
  ];
  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      if(_selectedIndex==0)
        {
          title="Home";
        }
      if(_selectedIndex==1)
      {
        title="Matche";
      }
      if(_selectedIndex==2)
      {
        title="Chat";
      }
      if(_selectedIndex==3)
      {
        title="My Account";
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    AppUtils.chnageStatusBar(Colors.red);
    return Scaffold(
      backgroundColor: ColorUtils.colorBg,
      appBar: AppUtils.toolbarwithText(context, title),
      body:  welcomeImages1.elementAt(_selectedIndex),

      bottomNavigationBar: CurvedNavigationBar(
        color:Color(0xff383D40),
        backgroundColor: Colors.transparent,
        onTap: _onItemTapped,
        buttonBackgroundColor: Colors.white,
        height: 70.0,
        items: <Widget>[
          Icon(
            Icons.home,
            size: 30.0,
            color: ColorUtils.colorRedNew,
          ),
          Icon(
            Icons.favorite,
            size: 30.0,
            color: ColorUtils.colorRedNew,
          ),
          Icon(
              Icons.chat_bubble,
            size:   30.0,
            color: ColorUtils.colorRedNew,
          ),
          Icon(
            Icons.person,
            size: 30.0,
            color: ColorUtils.colorRedNew,
          ),//

        ],
      ),

    );
  }

}
