import 'package:dating_app/utils/AppUtils.dart';
import 'package:dating_app/utils/ColorUtils.dart';
import 'package:dating_app/utils/WidgetUtils.dart';
import 'package:flutter/material.dart';

class ForgotPassword extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ForgotPasswordState();
  }
  
}
class ForgotPasswordState extends State<ForgotPassword>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: ColorUtils.colorBg,
      appBar: AppUtils.toolbarwithText(context, "Forgot password".toUpperCase()),
      body: viewSingup(),
    );
  }
  Widget viewSingup(){
    return Container(


      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 40,),
          Container(
            margin: EdgeInsets.only(top: 15,left: 15,right: 15),
            child:     TextFormField(
              style: WidgetUtils.textStyleView(Colors.white),
              decoration:WidgetUtils.inputDecoration(Colors.white,Icons.person,Colors.white,"Email"),
              keyboardType: TextInputType.emailAddress,

            ),
          ),
          SizedBox(height: 20,),


          SizedBox(height: 30,),
          TextButton(onPressed: (){
            // Navigator.push(context, MaterialPageRoute(builder: (context)=>Dashboard()));
          }, child:Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              elevation: 0,
              margin: EdgeInsets.only(left: 20,right: 20),
              color: ColorUtils.colorBflight,
              child:  Container(
                alignment: Alignment.center,

                padding: EdgeInsets.all(15),

                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: ColorUtils.colorBflight
                ),
                child: Text("Send",style: WidgetUtils.textStyleViewBold(Colors.white, 14)),
              )),
          ),



        ],
      ),
    );
  }
}