import 'package:dating_app/utils/AppUtils.dart';
import 'package:dating_app/utils/ColorUtils.dart';
import 'package:dating_app/utils/WidgetUtils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SettingScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SettingScreenState();
  }
  
}
class SettingScreenState extends State<SettingScreen>{
  bool isSwitched = false;
  RangeValues _currentRangeValues = const RangeValues(40, 80);

  int valueHolder = 20;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: ColorUtils.colorBg,
      appBar: AppUtils.toolbarwithText(context, "Settings"),
      body: SingleChildScrollView(
        child: Container(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                  InkWell(
                    child:Container(
                      margin: EdgeInsets.all(10),
                      child:  Card(
                        color: ColorUtils.colorBflight,
                        child: Container(

                          padding: EdgeInsets.all(10),
                          child: Column(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                 children: [

                                  SvgPicture.asset("assets/images/explore_icon.svg", width: 30,height: 30,color: Colors.white,),
                                  SizedBox(width: 10,),
                                  Text("Tinder",style: WidgetUtils.textStyleViewBold(Colors.white, 22),),
                                   SizedBox(width: 10,),
                                  Container(

                                    padding: EdgeInsets.only(left: 20,right: 20,top: 5,bottom: 5),
                                    child:  Text("Platinum",style: WidgetUtils.textStyleViewBold(Colors.white, 16),),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(topLeft: Radius.circular(10),bottomRight: Radius.circular(10)),
                                      color: ColorUtils.colorRedNew ,
                                      shape: BoxShape.rectangle,

                                    ),
                                  )
                                ],
                              ),
                              SizedBox(height: 5,),
                              Text("Priority Likes, See who likes you & more",style: WidgetUtils.textStyleViewBold(Colors.grey, 16),),
                            ],
                          ),
                        )
                      ),
                    )
                  ),
                InkWell(
                    child:Container(
                      margin: EdgeInsets.all(10),
                      child:  Card(
                          color: ColorUtils.colorBflight,
                          child: Container(

                            padding: EdgeInsets.all(10),
                            child: Column(
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [

                                    SvgPicture.asset("assets/images/explore_icon.svg", width: 30,height: 30,color:    Color(0xffd4af37),),
                                    SizedBox(width: 10,),
                                    Text("Tinder",style: WidgetUtils.textStyleViewBold(Colors.white, 22),),
                                    SizedBox(width: 10,),
                                    Container(

                                      padding: EdgeInsets.only(left: 20,right: 20,top: 5,bottom: 5),
                                      child:  Text("Gold",style: WidgetUtils.textStyleViewBold(Colors.white, 16),),
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                          colors: [
                                            Color(0xffd4af37),
                                            Color(0xff95FFD700),
                                          ],
                                        ),
                                        borderRadius: BorderRadius.only(topLeft: Radius.circular(10),bottomRight: Radius.circular(10)),
                                        color: Colors.black87 ,
                                        shape: BoxShape.rectangle,

                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(height: 5,),
                                Text("See who likes you & more",style: WidgetUtils.textStyleViewBold(Colors.grey, 16),),
                              ],
                            ),
                          )
                      ),
                    )
                ),
                InkWell(
                    child:Container(
                      margin: EdgeInsets.all(10),
                      child:  Card(
                          color: ColorUtils.colorBflight,
                          child: Container(

                            padding: EdgeInsets.all(10),
                            child: Column(
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [

                                    SvgPicture.asset("assets/images/explore_icon.svg", width: 24,height: 24,color:ColorUtils.colorRedNew,),
                                    SizedBox(width: 10,),
                                    Text("Tinder",style: WidgetUtils.textStyleViewBold(Colors.white, 22),),
                                    SizedBox(width: 10,),
                                    Icon(Icons.add,color: ColorUtils.colorRedNew,)
                                  ],
                                ),
                                SizedBox(height: 5,),
                                Text("Unlimited Likes & More",style: WidgetUtils.textStyleViewBold(Colors.grey, 16),),
                              ],
                            ),
                          )
                      ),
                    )
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Flexible(
                      fit: FlexFit.tight,
                        flex: 50  ,
                        child:InkWell(


                          child:  Card(
                              color: ColorUtils.colorBflight,
                              margin: EdgeInsets.all(12),
                              child: Container(
                                height: 120,
                                padding: EdgeInsets.all(15),

                                child: Column(
                                  children: [

                                    FloatingActionButton(
                                      heroTag:"like",onPressed: (){},
                                      backgroundColor: Colors.white,
                                    child: Icon(Icons.star,size: 24,color: ColorUtils.colorRedNew,),),
                                    SizedBox(height: 5,),
                                    Text("Get Super likes",style: WidgetUtils.textStyleViewBold(ColorUtils.colorRedNew, 16),),
                                  ],
                                ),
                              )
                          ),
                        )
                    ),
                    Flexible(
                        fit: FlexFit.tight,
                        flex: 50  ,
                        child:InkWell(


                          child:  Card(
                            color: ColorUtils.colorBflight,
                              margin: EdgeInsets.all(12),
                              child: Container(
                                alignment: Alignment.center,
                                height: 120,

                                padding: EdgeInsets.all(15),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [

                                    FloatingActionButton(
                                        heroTag:"Boosts",onPressed: (){},
                                      backgroundColor: Colors.white,
                                      child: SvgPicture.asset("assets/images/thunder_icon.svg",width: 24,height: 24,color: ColorUtils.colorRedNew,)),
                                    SizedBox(height: 5,),
                                    Text("Get Boosts",style: WidgetUtils.textStyleViewBold(ColorUtils.colorRedNew, 16),),
                                  ],
                                ),
                              )
                          ),
                        )
                    ),
                  ],
                ),
                InkWell(
                    child:Container(
                      margin: EdgeInsets.all(8),
                      child:  Card(
                        color: ColorUtils.colorBflight,
                          child: Container(

                            padding: EdgeInsets.all(16),
                            child: Column(
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [


                                    Image.asset("assets/img/vip.png", width: 24,height: 24,color:ColorUtils.colorRedNew,),
                                    SizedBox(width: 10,),
                                    Text("Get Rewards",style: WidgetUtils.textStyleViewBold(Colors.white, 18),),

                                  ],
                                ),
                                SizedBox(height: 5,),

                              ],
                            ),
                          )
                      ),
                    )
                ),
                Container(
                  margin: EdgeInsets.only(left: 15),
                  child:  Text("Account Settings",style: WidgetUtils.textStyleViewBold(Colors.grey, 16),),
                ),
               InkWell(

                 onTap: (){

                 },
                 child:  Card(
                   margin: EdgeInsets.all(10),
                   color: ColorUtils.colorBflight,
                   child: Container(

                       padding: EdgeInsets.all(16),
                       child: Row(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                         children: [
                           Row(
                             children: [
                               Text("Phone Number",style: WidgetUtils.textStyleView1(Colors.grey, 16),),
                             ],
                           ),
                           SizedBox(width: 10,),
                           Row(
                             children: [
                               Text("+917383044605",style: WidgetUtils.textStyleView1 (Colors.grey, 16),),
                               Icon(Icons.arrow_forward_ios,size: 20,color: Colors.grey,)
                             ],
                           )
                         ],
                       ),
                   ),

                 ),
               ),
                Container(
                  margin: EdgeInsets.only(left: 15),
                  child:  Text("Verify a phone number to help secure your account",style: WidgetUtils.textStyleViewBold(Colors.grey, 14),),
                ),
                SizedBox(height: 20,),
                Container(
                  margin: EdgeInsets.only(left: 15),
                  child:  Text("Discover Settings",style: WidgetUtils.textStyleViewBold(Colors.grey, 16),),
                ),
                Container(


                  child:  Card(
                    margin: EdgeInsets.all(10),
                    color: ColorUtils.colorBflight,
                    child: Container(

                      padding: EdgeInsets.all(16),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Text("Location",style: WidgetUtils.textStyleView1(Colors.grey, 16),),
                            ],
                          ),
                          SizedBox(width: 10,),
                          Row(
                            children: [
                              InkWell(
                                child:   Text("Cureent location",style: WidgetUtils.textStyleViewBold (Colors.blue, 16),),
                              )


                            ],
                          )
                        ],
                      ),
                    ),

                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 15),
                  child:  Text("Change your location to see tinder member in other cities.",style: WidgetUtils.textStyleViewBold(Colors.grey, 14),),
                ),
                SizedBox(height: 20,),
                Container(

                margin: EdgeInsets.only(left: 10,right: 10),
                  child:  Card(
                 
                    color: ColorUtils.colorBflight,
                    child: Container(
                      height: 56,
                      alignment: Alignment.center,

                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(

                            children: [

                           Container(
                             margin: EdgeInsets.only(left: 10),

                             child:    Text("Global",textAlign:TextAlign.center, style: WidgetUtils.textStyleView1(Colors.grey, 16),),
                           )

                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [

                              Switch(
                                  activeTrackColor: Colors.white,
                                  activeColor: Colors.green,
                                  value: isSwitched, onChanged: (value){
                                setState(() {
                                  isSwitched=value;
                                });
                              })
                            ],
                          ),

                        ],
                      ),
                    ),

                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 15),
                  child:  Text("Going global will allow you to see people nearby and from around the world",style: WidgetUtils.textStyleViewBold(Colors.grey, 14),),
                ),
                SizedBox(height: 20,),
                Container(

                  margin: EdgeInsets.only(left: 10,right: 10),
                  child:  Card(

                    color: ColorUtils.colorBflight,
                    child: Container(
                      height: 130,
                      alignment: Alignment.center,

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(

                                children: [

                                  Container(
                                    margin: EdgeInsets.only(left: 20,right: 20),

                                    child:    Text("Maximum Distance",textAlign:TextAlign.center, style: WidgetUtils.textStyleViewBold(ColorUtils.colorRedNew, 16),),
                                  ),



                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [

                                  Container(
                                    margin: EdgeInsets.only(left: 20,right: 20),

                                    child:    Text("12 mi.",textAlign:TextAlign.center, style: WidgetUtils.textStyleViewBold(Colors.grey, 16),),
                                  ),
                                ],
                              ),

                            ],
                          ),
                          SizedBox(height: 10,),
                         Flexible(child:  Container(
                           height: 30,

                             child: Slider(
                                 value: valueHolder.toDouble(),
                                 min: 1,
                                 max: 100,
                                 divisions: 100,

                                 activeColor: ColorUtils.colorRedNew,
                                 inactiveColor:  ColorUtils.colorButton,
                                 label: '${valueHolder.round()}',
                                 onChanged: (double newValue) {
                                   setState(() {
                                     valueHolder = newValue.round();
                                   });
                                 },
                                 semanticFormatterCallback: (double newValue) {
                                   return '${newValue.round()}';
                                 }
                             )),),
                          Container(

                            child: Container(
                                child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Row(

                                    children: [

                                      Container(
                                        margin: EdgeInsets.only(left: 20,right: 20),

                                        child:    Text("Only show people in this range",textAlign:TextAlign.center, style: WidgetUtils.textStyleView1(Colors.grey, 16),),
                                      )

                                    ],
                                  ),
                                  Row(

                                    children: [

                                      Switch(
                                          activeTrackColor: Colors.white,
                                          activeColor: Colors.green,
                                          value: isSwitched, onChanged: (value){
                                        setState(() {
                                          isSwitched=value;
                                        });
                                      })
                                    ],
                                  ),

                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                  ),
                ),
                SizedBox(height: 20,),
                Container(

                  margin: EdgeInsets.only(left: 10,right: 10),
                  child:  Card(

                    color: ColorUtils.colorBflight,
                    child: Container(

                      padding: EdgeInsets.all(10),
                      alignment: Alignment.center,

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(

                                children: [

                                  Container(
                                    margin: EdgeInsets.only(left: 20,right: 20),

                                    child:    Text("Show Me",textAlign:TextAlign.center, style: WidgetUtils.textStyleViewBold(ColorUtils.colorRedNew, 16),),
                                  ),



                                ],
                              ),


                            ],
                          ),

                          Container(
                            padding: EdgeInsets.all(5),
                            child: Container(

                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Row(

                                    children: [

                                      Container(
                                        margin: EdgeInsets.only(left: 15,right: 20),

                                        child:    Text("Women",textAlign:TextAlign.center, style: WidgetUtils.textStyleView1(Colors.grey, 16),),
                                      )

                                    ],
                                  ),
                                  Row(

                                    children: [
                                      Container(
                                        padding: EdgeInsets.only(bottom: 10),
                                     //   margin: EdgeInsets.only(left: 20,right: 20),

                                        child:      Icon(Icons.arrow_forward_ios,size: 20,color: Colors.grey,)
                                      )

                                    ],
                                  ),

                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                  ),
                ),
                Container(

                  margin: EdgeInsets.only(left: 10,right: 10),
                  child:  Card(

                    color: ColorUtils.colorBflight,
                    child: Container(
                      height: 130,
                      alignment: Alignment.center,

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(

                                children: [

                                  Container(
                                    margin: EdgeInsets.only(left: 20,right: 20),

                                    child:    Text("Age Range",textAlign:TextAlign.center, style: WidgetUtils.textStyleViewBold(ColorUtils.colorRedNew, 16),),
                                  ),



                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [

                                  Container(
                                    margin: EdgeInsets.only(left: 20,right: 20),

                                    child:    Text("18 - 31",textAlign:TextAlign.center, style: WidgetUtils.textStyleViewBold(Colors.grey, 16),),
                                  ),
                                ],
                              ),

                            ],
                          ),
                          SizedBox(height: 10,),
                          Flexible(child:  Container(
                              height: 30,

                              child: RangeSlider(
                                inactiveColor: ColorUtils.colorButton,
                                activeColor: ColorUtils.colorRedNew,
                                values: _currentRangeValues,   max: 100,
                                divisions: 5,
                                labels: RangeLabels(
                                  _currentRangeValues.start.round().toString(),
                                  _currentRangeValues.end.round().toString(),
                                ),
                                onChanged: (RangeValues vale){
                                  setState(() {
                                    _currentRangeValues=vale;
                                  });
                                },
                                
                              )),),
                          Container(

                            child: Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Row(

                                    children: [

                                      Container(
                                        margin: EdgeInsets.only(left: 20,right: 20),

                                        child:    Text("Only show people in this range",textAlign:TextAlign.center, style: WidgetUtils.textStyleView1(Colors.grey, 16),),
                                      )

                                    ],
                                  ),
                                  Row(

                                    children: [

                                      Switch(
                                          activeTrackColor: Colors.white,
                                          activeColor: Colors.green,
                                          value: isSwitched, onChanged: (value){
                                        setState(() {
                                          isSwitched=value;
                                        });
                                      })
                                    ],
                                  ),

                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                  ),
                ),
                SizedBox(height: 20,),

                Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 15),
                      child:  Text("Control Who You See",style: WidgetUtils.textStyleViewBold(Colors.grey, 16),),
                    ),
                    SizedBox(width: 10,),
                    Container(

                      padding: EdgeInsets.only(left: 10,right: 10,top: 5,bottom: 5),
                      child:  Text("Tinder Plus",style: WidgetUtils.textStyleViewBold(Colors.white, 14),),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        color: ColorUtils.colorRedNew ,
                        shape: BoxShape.rectangle,

                      ),
                    )
                  ],
                ),


              ],
            ),
          ),
        ),
      ),

    );
  }
  
}