import 'package:dating_app/login/Login.dart';
import 'package:dating_app/utils/AppUtils.dart';
import 'package:dating_app/utils/ColorUtils.dart';
import 'package:dating_app/utils/WidgetUtils.dart';
import 'package:flutter/material.dart';

class SignupScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SignupScreenState();
  }
  
}
class SignupScreenState extends State<SignupScreen>{
  @override
  Widget build(BuildContext context) {
    AppUtils.chnageStatusBar(ColorUtils.colorBg);
    return Scaffold(
        backgroundColor: ColorUtils.colorBg,
        appBar: AppUtils.toolbarwithText(context,"Sign Up"),
        bottomNavigationBar: Container(

            color: ColorUtils.colorBg,
            margin: EdgeInsets.only(bottom: 20),
            child:  TextButton(
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>Login()));
              },
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Alredy have an account?",textAlign:TextAlign.center,style: WidgetUtils.textStyleView1(Colors.white, 14)),
                  Text("Sign In",textAlign:TextAlign.center,style: WidgetUtils.textStyleViewBold(Colors.white, 14)),
                ],
              ),
            )
        ),
        body: Center(
          child: SingleChildScrollView(

            child: viewSingup(),
          ),
        )
    );
  }

  Widget viewSingup(){
    return Container(


      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 40,),
          Container(
            margin: EdgeInsets.only(top: 15,left: 15,right: 15),
            child:     TextFormField(
              style: WidgetUtils.textStyleView(Colors.white),
              decoration:WidgetUtils.inputDecoration(Colors.white,Icons.person,Colors.white,"Full Name"),
              keyboardType: TextInputType.name,

            ),
          ),
          SizedBox(height: 20,),
          Container(
            margin: EdgeInsets.only(top: 15,left: 15,right: 15),
            child:     TextFormField(
              style: WidgetUtils.textStyleView(Colors.white),
              decoration:WidgetUtils.inputDecoration(Colors.white,Icons.phone_android,Colors.white,"Phone Number"),
              keyboardType: TextInputType.phone,

            ),
          ),
          SizedBox(height: 20,),

          Container(
            margin: EdgeInsets.only(top: 15,left: 15,right: 15),
            child:     TextFormField(
              style: WidgetUtils.textStyleView(Colors.white),
              decoration:WidgetUtils.inputDecoration(Colors.white,Icons.email,Colors.white,"Email"),
              keyboardType: TextInputType.emailAddress,

            ),
          ),
          SizedBox(height: 20,),

          Container(

            margin: EdgeInsets.only(top: 15,left: 15,right: 15),
            child:     TextFormField(
              obscureText: true,
              keyboardType: TextInputType.visiblePassword,
              style: WidgetUtils.textStyleView(Colors.white),
              decoration:WidgetUtils.inputDecoration(Colors.white,Icons.lock,Colors.white,"Password"),

            ),
          ),
          SizedBox(height: 20,),
          Container(

            margin: EdgeInsets.only(top: 15,left: 15,right: 15),
            child:     TextFormField(
              obscureText: true,
              keyboardType: TextInputType.visiblePassword,
              style: WidgetUtils.textStyleView(Colors.white),
              decoration:WidgetUtils.inputDecoration(Colors.white,Icons.lock,Colors.white,"Confirm Password"),

            ),
          ),

          SizedBox(height: 30,),
          TextButton(onPressed: (){
            // Navigator.push(context, MaterialPageRoute(builder: (context)=>Dashboard()));
          }, child:Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              elevation: 0,
              margin: EdgeInsets.only(left: 20,right: 20),
              color: ColorUtils.colorBflight,
              child:  Container(
                alignment: Alignment.center,

                padding: EdgeInsets.all(15),

                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: ColorUtils.colorBflight
                ),
                child: Text("Sing Up",style: WidgetUtils.textStyleViewBold(Colors.white, 14)),
              )),
          ),





        ],
      ),
    );
  }
}