import 'dart:async';

import 'package:dating_app/login/Login.dart';
import 'package:dating_app/utils/AppUtils.dart';
import 'package:dating_app/utils/ColorUtils.dart';
import 'package:dating_app/utils/WidgetUtils.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SplashScreenState();
  }
  
}
class SplashScreenState extends State<SplashScreen>{
  @override
  void initState() {
    super.initState();
    gotoLogin();
  }
  @override
  Widget build(BuildContext context) {
    AppUtils.chnageStatusBar(ColorUtils.colorBg);
    return Scaffold(
        backgroundColor: ColorUtils.colorBg,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Welcome",style: WidgetUtils.textStyleViewBold(Colors.white, 20)),
            SizedBox(height: 20,),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.all(10),
              child:  Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever..",textAlign: TextAlign.center,style: WidgetUtils.textStyleView1(Colors.white, 16)),
            ),

          ],
        ),
      ),
    );
  }
  gotoLogin(){
    Timer(Duration(seconds: 5),()=>Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Login())));
  }
}