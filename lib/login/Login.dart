
import 'package:dating_app/ForgotPassword.dart';
import 'package:dating_app/Signup/SignupScreen.dart';
import 'package:dating_app/dashboard/Dashboard.dart';
import 'package:dating_app/utils/AppUtils.dart';
import 'package:dating_app/utils/ColorUtils.dart';
import 'package:dating_app/utils/WidgetUtils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginState();
  }
  
}
class LoginState extends State<Login>{
  @override
  Widget build(BuildContext context) {
    AppUtils.chnageStatusBar(ColorUtils.colorBg);
    return Scaffold(
        backgroundColor: ColorUtils.colorBg,
        appBar: AppUtils.toolbarwithText(context,"Sign In"),
        bottomNavigationBar: Container(

          color: ColorUtils.colorBg,
          margin: EdgeInsets.only(bottom: 20),
          child:  TextButton(
            onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>SignupScreen()));
            },
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Don't have an account?",textAlign:TextAlign.center,style: WidgetUtils.textStyleView1(Colors.white, 14)),
                Text("Sign Up",textAlign:TextAlign.center,style: WidgetUtils.textStyleViewBold(Colors.white, 14)),
              ],
            ),
          )
        ),
        body: Center(
          child: SingleChildScrollView(

            child: viewSingup(),
          ),
        )
    );
  }

  Widget viewSingup(){
    return Container(


      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 40,),
          Container(
            margin: EdgeInsets.only(top: 15,left: 15,right: 15),
            child:     TextFormField(
              style: WidgetUtils.textStyleView(Colors.white),
              decoration:WidgetUtils.inputDecoration(Colors.white,Icons.email,Colors.white,"Email"),
              keyboardType: TextInputType.emailAddress,

            ),
          ),
          SizedBox(height: 20,),
          Container(

            margin: EdgeInsets.only(top: 15,left: 15,right: 15),
            child:     TextFormField(
              obscureText: true,
              keyboardType: TextInputType.visiblePassword,
              style: WidgetUtils.textStyleView(Colors.white),
              decoration:WidgetUtils.inputDecoration(Colors.white,Icons.person,Colors.white,"Password"),

            ),
          ),
          TextButton(
            onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>ForgotPassword()));
            },
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text("Forgot Password?",textAlign:TextAlign.center,style: WidgetUtils.textStyleView1(Colors.white, 14)),
              ],
            ),
          ),
          SizedBox(height: 30,),
          TextButton(onPressed: (){
           Navigator.push(context, MaterialPageRoute(builder: (context)=>Dashboard()));
          }, child:Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              elevation: 0,
              margin: EdgeInsets.only(left: 20,right: 20),
              color: ColorUtils.colorBflight,
              child:  Container(
                alignment: Alignment.center,

                padding: EdgeInsets.all(15),

                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: ColorUtils.colorBflight
                ),
                child: Text("Log In",style: WidgetUtils.textStyleViewBold(Colors.white, 14)),
              )),
          ),
          SizedBox(height: 30,),
          Divider(
            height: 2,
            color: Colors.white,
          ),
          SizedBox(height: 20,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text("- OR -",style: WidgetUtils.textStyleView1(Colors.white, 14)),
              SizedBox(height: 10,),
              Text("Sign with",style: WidgetUtils.textStyleView1(Colors.white, 14)),

            ],
          ),
          SizedBox(height: 20,),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              TextButton(onPressed: (){
                // Navigator.push(context, MaterialPageRoute(builder: (context)=>Dashboard()));
              }, child:Card(

                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(80),
                  ),
                  elevation: 5,
                  margin: EdgeInsets.only(left: 20,right: 0),
                  color: ColorUtils.colorGoogle,
                  child:  Container(
                    alignment: Alignment.center,
                    width: 70,
                    height: 70  ,
                    padding: EdgeInsets.all(15),

                    child: Image.asset("assets/img/ic_google.png",color: Colors.white,width: 30,height: 30,)
                  )),
              ),
              TextButton(onPressed: (){
                // Navigator.push(context, MaterialPageRoute(builder: (context)=>Dashboard()));
              }, child:Card(

                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(80),
                  ),
                  elevation: 5,
                  margin: EdgeInsets.only(left: 0,right: 0),
                  color: ColorUtils.colorFB,
                  child:  Container(
                      alignment: Alignment.center,
                      width: 70,
                      height: 70,
                      padding: EdgeInsets.all(15),
                      child: Image.asset("assets/img/ic_facebook.png",color: Colors.white,width: 30,height: 30,)
                  )),
              ),
              TextButton(onPressed: (){
                // Navigator.push(context, MaterialPageRoute(builder: (context)=>Dashboard()));
              }, child:Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(80),
                  ),
                  elevation: 5,
                  margin: EdgeInsets.only(left: 0,right: 20),
                  color: ColorUtils.colorTwitter,
                  child:  Container(
                      alignment: Alignment.center,
                      width: 70,
                      height: 70,
                      padding: EdgeInsets.all(15),
                      child: Image.asset("assets/img/ic_twitter.png",color: Colors.white,width: 30,height: 30,)
                  )),
              ),
            ],
          ),
         


        ],
      ),
    );
  }
}