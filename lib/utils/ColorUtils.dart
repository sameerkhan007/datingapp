import 'package:flutter/cupertino.dart';

class ColorUtils{
  static Color colorBg=Color(0xff292D30);
  static Color colorBflight=Color(0xff383C3F);
  static Color colorButton=Color(0xff4A494C);
  static Color colorLightBlue=Color(0xffC9DBFF);
  static Color colorLightPink=Color(0xffDDC3DC);
  static Color colorLightYellow=Color(0xffFBDEBC);
  static Color colorLightGreen=Color(0xffD6E4D3);
  static Color colorLightYellow1=Color(0xffF7F1CF);
  static Color colorFB=Color(0xff4267B2);
  static Color colorGoogle=Color(0xffDB4437);
  static Color colorTwitter=Color(0xff00acee);
  static Color colorRedNew=Color(0xffF6445D);
  static Color colorLightRed=Color(0xffEDC8B8);
}