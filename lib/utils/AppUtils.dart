import 'package:dating_app/utils/ColorUtils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppUtils {
  static String fontSatoshi="Satoshi";
  static String fontTelma="Telma";
  static AppBar toolbarwithText(BuildContext context,String title)
  {
    return AppBar(

      backgroundColor: ColorUtils.colorBg,
      elevation: 0,
      leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios_outlined,color: Colors.white,),
          onPressed: (){Navigator.pop(context,true);}
      ),
      centerTitle: true,
      title: Text(title.toUpperCase(),style: TextStyle(color: Colors.white,fontSize: 16,fontWeight: FontWeight.w800,fontFamily: AppUtils.fontSatoshi),),
      automaticallyImplyLeading: true,

    );


  }
  static  chnageStatusBar(Color colorCode){
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor:colorCode,
      systemNavigationBarColor: colorCode,
      statusBarBrightness: Brightness.dark,
    ));
  }



}