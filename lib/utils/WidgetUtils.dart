
import 'package:dating_app/dashboard/chat/ChatScreen.dart';
import 'package:dating_app/model/DataBean.dart';
import 'package:dating_app/utils/ColorUtils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:getwidget/components/card/gf_card.dart';
import 'package:getwidget/components/carousel/gf_carousel.dart';

import 'AppUtils.dart';

class WidgetUtils{

  static List<DataBean> notificationData = [

      DataBean("Calender", "Your staff meeting starts in 15 mins", "assets/bell.png","5 mins ago"),
    DataBean("SALES", "An order of \$120 has been placed.", "assets/bell.png","5 mins ago"),
    DataBean("RECOMMENDATIONS", "New recommendation for Jone Doe has been prepared.", "assets/bell.png","5 mins ago"),
    DataBean("USERS ", "A new account has been created.", "assets/bell.png","5 mins ago"),
    DataBean("MONITORING ", "Anomaly detected! Your landing page has spiked 10% in page views in the lasthour.", "assets/bell.png","5 mins ago"),
    DataBean("ALERT ", "You site has been down for 5 minutes..", "assets/bell.png","5 mins ago"),
  ];

  static List<DataBean> activityFeed = [

    DataBean("Order Placed", "#3214235 . Sep 21", "assets/bell.png","#3214235"),
    DataBean("Payment Received", "Sep 21", "assets/bell.png","\$198.60"),
    DataBean("Product Dispatched", "Strip #3214235 . #Sep 21", "assets/bell.png","#3214235"),
    DataBean("Product Delivered ", "#3214235 . #Sep 21", "assets/bell.png","#3214235"),
    DataBean("Refund Processed ", "Sep 21", "assets/bell.png","\$142.00"),
    DataBean("Order Placed", "#3214235 . Sep 21", "assets/bell.png","#3214235"),
    DataBean("Payment Received", "Sep 21", "assets/bell.png","\$198.60"),
    DataBean("Product Dispatched", "Strip #3214235 . #Sep 21", "assets/bell.png","#3214235"),
    DataBean("Product Delivered ", "#3214235 . #Sep 21", "assets/bell.png","#3214235"),
    DataBean("Refund Processed ", "Sep 21", "assets/bell.png","\$142.00"),
  ];
  static List<DataBean> prodctus = [

    DataBean("Zapatos Shows", "The Black 9 Shoes look", "assets/ic_shose1.jpg","Created Jil 12"),
    DataBean("Adidas Sweatshirt", "Take it back to classic", "assets/ic_shose1.jpg","Created Jil 12"),
    DataBean("Woodland Shoes", "Best Seller", "assets/ic_shose1.jpg","Created Jil 12"),
    DataBean("Zapatos Shows ", "The Black 9 Shoes look", "assets/ic_shose1.jpg","Created Jil 12"),
    DataBean("Adidas Sweatshirt ", "Take it back to classic", "assets/ic_shose1.jpg","Created Jil 12"),
    DataBean("Zapatos Shows", "The Black 9 Shoes look", "assets/ic_shose1.jpg","Created Jil 12"),
    DataBean("Adidas Sweatshirt", "Take it back to classic", "assets/ic_shose1.jpg","Created Jil 12"),
    DataBean("Woodland Shoes", "Best Seller", "assets/ic_shose1.jpg","Created Jil 12"),
    DataBean("Zapatos Shows ", "The Black 9 Shoes look", "assets/ic_shose1.jpg","Created Jil 12"),
    DataBean("Adidas Sweatshirt ", "Take it back to classic", "assets/ic_shose1.jpg","Created Jil 12"),
  ];
  static List<DataBean> recent = [

    DataBean("Zapatos Shows", "The Black 9 Shoes look", "assets/ic_shose1.jpg","\$200"),
    DataBean("Adidas Sweatshirt", "Take it back to classic", "assets/ic_shose1.jpg","\$300"),
    DataBean("Woodland Shoes", "Best Seller", "assets/ic_shose1.jpg","\$200"),
    DataBean("Zapatos Shows ", "The Black 9 Shoes look", "assets/ic_shose1.jpg","\$200"),
    DataBean("Adidas Sweatshirt ", "Take it back to classic", "assets/ic_shose1.jpg","\$200"),
    DataBean("Zapatos Shows", "The Black 9 Shoes look", "assets/ic_shose1.jpg","\$200"),
    DataBean("Adidas Sweatshirt", "Take it back to classic", "assets/ic_shose1.jpg","\$200"),
    DataBean("Woodland Shoes", "Best Seller", "assets/ic_shose1.jpg","\$200"),
    DataBean("Zapatos Shows ", "The Black 9 Shoes look", "assets/ic_shose1.jpg","\$200"),
    DataBean("Adidas Sweatshirt ", "Take it back to classic", "assets/ic_shose1.jpg","\$200"),
  ];

  static List<DataBean> taskList = [

    DataBean("Launch Android app for Dashboard", " Due Date: Sep 21", "assets/bell.png","In Progress"),
    DataBean("Publish new product page", "Due Date: Sep 21", "assets/bell.png","Done"),
    DataBean("Redesign Landing Screen", "Due Date: Sep 21", "assets/bell.png","Not Started"),
    DataBean("Create product category for baby products ", "Due Date: Sep 21", "assets/bell.png","Not Started"),
    DataBean("Launch Android app for Dashboard", " Due Date: Sep 21", "assets/bell.png","In Progress"),
    DataBean("Publish new product page", "Due Date: Sep 21", "assets/bell.png","Done"),
    DataBean("Redesign Landing Screen", "Due Date: Sep 21", "assets/bell.png","Not Started"),
    DataBean("Create product category for baby products ", "Due Date: Sep 21", "assets/bell.png","Not Started"),
    DataBean("Launch Android app for Dashboard", " Due Date: Sep 21", "assets/bell.png","In Progress"),
    DataBean("Publish new product page", "Due Date: Sep 21", "assets/bell.png","Done"),
    DataBean("Redesign Landing Screen", "Due Date: Sep 21", "assets/bell.png","Not Started"),
    DataBean("Create product category for baby products ", "Due Date: Sep 21", "assets/bell.png","Not Started"),
  ];

  static List<DataBean> customer = [

    DataBean("Launch Android app for Dashboard", " Due Date: Sep 21", "assets/ic_man.jpg","In Progress"),
    DataBean("Publish new product page", "Due Date: Sep 21", "assets/ic_man.jpg","Done"),
    DataBean("Redesign Landing Screen", "Due Date: Sep 21", "assets/ic_man.jpg","Not Started"),
    DataBean("Create product category for baby products ", "Due Date: Sep 21", "assets/ic_man.jpg","Not Started"),
    DataBean("Launch Android app for Dashboard", " Due Date: Sep 21", "assets/ic_man.jpg","In Progress"),
    DataBean("Publish new product page", "Due Date: Sep 21", "assets/ic_man.jpg","Done"),
    DataBean("Redesign Landing Screen", "Due Date: Sep 21", "assets/ic_man.jpg","Not Started"),
    DataBean("Create product category for baby products ", "Due Date: Sep 21", "assets/ic_man.jpg","Not Started"),
    DataBean("Launch Android app for Dashboard", " Due Date: Sep 21", "assets/ic_man.jpg","In Progress"),
    DataBean("Publish new product page", "Due Date: Sep 21", "assets/ic_man.jpg","Done"),
    DataBean("Redesign Landing Screen", "Due Date: Sep 21", "assets/ic_man.jpg","Not Started"),
    DataBean("Create product category for baby products ", "Due Date: Sep 21", "assets/ic_man.jpg","Not Started"),
  ];
  static List<DataBean> customerList = [

    DataBean("Daniel Wellington", "iPhone • Total Spend \$2", "assets/ic_user.jpg","Created Jul 12"),
    DataBean("Jhon Wellington", "iPhone • Total Spend \$2", "assets/ic_user.jpg","Created Jul 12"),
    DataBean("Smits Wellington", "iPhone • Total Spend \$2", "assets/ic_user.jpg","Created Jul 12"),
    DataBean("Daniel Wellington", "iPhone • Total Spend \$2", "assets/ic_user.jpg","Created Jul 12"),
    DataBean("Narj Wellington", "iPhone • Total Spend \$2", "assets/ic_user.jpg","Created Jul 12"),
    DataBean("Daniel Wellington", "iPhone • Total Spend \$2", "assets/ic_user.jpg","Created Jul 12"),
    DataBean("Jhon Wellington", "iPhone • Total Spend \$2", "assets/ic_user.jpg","Created Jul 12"),
    DataBean("Smits Wellington", "iPhone • Total Spend \$2", "assets/ic_user.jpg","Created Jul 12"),
    DataBean("Daniel Wellington", "iPhone • Total Spend \$2", "assets/ic_user.jpg","Created Jul 12"),
    DataBean("Narj Wellington", "iPhone • Total Spend \$2", "assets/ic_user.jpg","Created Jul 12"),
  ];

  static OutlineInputBorder enabledBorder(Color color){
     return OutlineInputBorder(
       borderRadius: BorderRadius.circular(25),
       borderSide: BorderSide(color: color, width: 2),
     );
   }
   static TextStyle textStyleView(Color color){
    return TextStyle(color:color,fontSize: 14,fontFamily: AppUtils.fontSatoshi);
   }
  static TextStyle textStyleView1(Color color,double size){
    return TextStyle(color:color,fontSize: size,fontFamily: AppUtils.fontSatoshi);
  }
  static TextStyle textStyleViewBold(Color color,double size){
    return TextStyle(color:color,fontSize: size,fontFamily: AppUtils.fontSatoshi,fontWeight: FontWeight.bold);
  }
   static InputDecoration inputDecoration(Color colortext,IconData iconImage,Color colorImage,String hint){
    return   InputDecoration(
      prefixIcon: Padding(
        padding: EdgeInsets.all(0), // add padding to adjust icon
        child: Icon(iconImage,color: colorImage,),
      ),
      border: WidgetUtils.enabledBorder(Colors.white),
      enabledBorder: WidgetUtils.enabledBorder(ColorUtils.colorButton),
      hintStyle: TextStyle(color:colortext,fontFamily: AppUtils.fontSatoshi,fontSize: 14),
      hintText: hint,
      focusedBorder:OutlineInputBorder(
        borderSide: const BorderSide(color: Colors.white, width: 2.0),
        borderRadius: BorderRadius.circular(25.0),
      ),

    );
   }
   static Widget load(){
     return ListView.builder(
         shrinkWrap: true,

         itemCount:recent.length,itemBuilder: (context,index){
       return Card(

         elevation: 0,
         margin: EdgeInsets.all(5),
         child: Container(padding:EdgeInsets.all(5),
             child:ListTile(
                 title:Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: [
                     Text(recent[index].name.toString(),style: TextStyle(color: Colors.black87,fontSize: 14,fontWeight: FontWeight.bold),),
                     //Text("\$17.47",style: TextStyle(color: Colors.black87,fontSize: 14,fontWeight: FontWeight.normal),),

                   ],
                 ),
                 leading:ClipOval(

                   child:    Container(
                       width: 70.0,
                       height: 70.0,
                       decoration: new BoxDecoration(
                           shape: BoxShape.circle,
                           border: Border.all(color: Colors.black87,width: 2),
                           image: new DecorationImage(

                               fit: BoxFit.cover,

                               image: ExactAssetImage(recent[index].image.toString(),)
                           )
                       ))
                 ),
                 subtitle: Container(
                   margin: EdgeInsets.only(top: 10),

                   child: Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: [
                       Text(recent[index].desc.toString(),style: textStyleView1(ColorUtils.colorButton, 12),),
                       Text(recent[index].timeValue.toString(),style: textStyleView1(getColor1(index), 14),),

                     ],
                   ),//child: Text("Woodland Shoes.Jhone\n#Paypal 525225 SEP-21",style: textStyleView1(AppUtils.colorBg, 14),),
                 )

             )
         ),
       );
     });
   }
  static Widget loadRecent(){
    //final europeanCountries = ['Woodland Shoes', 'Skater Dress', 'Adidas Sweatshirt','Woodland Shoes', 'Skater Dress', 'Adidas Sweatshirt','Woodland Shoes', 'Skater Dress'];
    return ListView.builder(
        shrinkWrap: true,

        itemCount:recent.length,itemBuilder: (context,index){
      return Card(

        elevation: 0,

        child: Container(padding:EdgeInsets.all(5),
            child:ListTile(
                title:Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(recent[index].name.toString(),style: TextStyle(color: Colors.black87,fontSize: 14,fontWeight: FontWeight.bold),),
                  //  Text("\$17.47",style: TextStyle(color: Colors.black87,fontSize: 14,fontWeight: FontWeight.normal),),

                  ],
                ),
                leading:ClipOval(

                    child:    Container(
                        width: 70.0,
                        height: 70.0,
                        decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(color: Colors.black87,width: 2),
                            image: new DecorationImage(

                                fit: BoxFit.cover,

                                image: ExactAssetImage(prodctus[index].image,)
                            )
                        ))
                ),
                subtitle: Container(
                  margin: EdgeInsets.only(top: 10),

                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(recent[index].desc,style: textStyleView1(ColorUtils.colorButton, 12),),
                      Text(recent[index].timeValue,style: textStyleView1(getColor1(index), 14),),

                    ],
                  ),//child: Text("Woodland Shoes.Jhone\n#Paypal 525225 SEP-21",style: textStyleView1(AppUtils.colorBg, 14),),
                )

            )
        ),
      );
    });
  }

  static Widget loadNotification(){
    //final europeanCountries = ['Woodland Shoes', 'Skater Dress', 'Adidas Sweatshirt','Woodland Shoes', 'Skater Dress', 'Adidas Sweatshirt','Woodland Shoes', 'Skater Dress'];
    return ListView.builder(
        shrinkWrap: true,

        itemCount:notificationData.length,itemBuilder: (context,index){
      return Card(

        elevation: 0,

        child: Container(padding:EdgeInsets.all(5),
            child:ListTile(
                title:Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(notificationData[index].name,style: TextStyle(color: Colors.black87,fontSize: 14,fontWeight: FontWeight.bold),),
                    //  Text("\$17.47",style: TextStyle(color: Colors.black87,fontSize: 14,fontWeight: FontWeight.normal),),

                  ],
                ),
                leading:ClipOval(

                  child: Container(
                    decoration: new BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.grey.shade200  ,
                      border: Border.all(color: Colors.grey, width: 3.0, style: BorderStyle.solid),

                    ),
                    height: 100.0,
                    width: 100.0,
                    child: Center(
                        child: Image.asset(notificationData[index].image,height: 35,width: 35,color: Colors.grey)
                    ),
                  ),
                ),
                subtitle: Container(
                  margin: EdgeInsets.only(top: 10),

                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(child:   Text(notificationData[index].desc,style: textStyleView1(ColorUtils.colorButton, 12),),),
                      Flexible(child:   Text(notificationData[index].timeValue,style: textStyleView1(ColorUtils.colorButton, 12),),)



                    ],
                  ),//child: Text("Woodland Shoes.Jhone\n#Paypal 525225 SEP-21",style: textStyleView1(AppUtils.colorBg, 14),),
                )

            )
        ),
      );
    });
  }

  static Widget loadActivty(){
    //final europeanCountries = ['Woodland Shoes', 'Skater Dress', 'Adidas Sweatshirt','Woodland Shoes', 'Skater Dress', 'Adidas Sweatshirt','Woodland Shoes', 'Skater Dress'];
    return ListView.builder(
        shrinkWrap: true,

        itemCount:activityFeed.length,itemBuilder: (context,index){
      return Card(

        elevation: 0,

        child: Container(padding:EdgeInsets.all(5),
            child:ListTile(
                title:Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(activityFeed[index].name,style: TextStyle(color: Colors.black87,fontSize: 14,fontWeight: FontWeight.bold),),
                    //  Text("\$17.47",style: TextStyle(color: Colors.black87,fontSize: 14,fontWeight: FontWeight.normal),),

                  ],
                ),
                leading:ClipOval(

                  child: Container(
                    decoration: new BoxDecoration(
                      shape: BoxShape.circle,
                      color: getColor1(index)   ,
                      border: Border.all(color:getColor1(index), width: 3.0, style: BorderStyle.solid),

                    ),
                    height: 15.0,
                    width: 15.0,

                  ),
                ),
                subtitle: Container(
                  margin: EdgeInsets.only(top: 10),

                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(child:   Text(activityFeed[index].desc,style: textStyleView1(ColorUtils.colorButton, 12),),),
                      Flexible(child:   Text(activityFeed[index].timeValue,style: textStyleView1(ColorUtils.colorButton, 12),),)



                    ],
                  ),//child: Text("Woodland Shoes.Jhone\n#Paypal 525225 SEP-21",style: textStyleView1(AppUtils.colorBg, 14),),
                )

            )
        ),
      );
    });
  }
  static Widget loadProduct(){
    return ListView.builder(
        shrinkWrap: true,

        itemCount:prodctus.length,itemBuilder: (context,index){
      return Card(

        elevation: 0,

        child: Container(padding:EdgeInsets.all(5),
            child:ListTile(
                title:Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(prodctus[index].name,style: TextStyle(color: Colors.black87,fontSize: 14,fontWeight: FontWeight.bold),),
                    //  Text("\$17.47",style: TextStyle(color: Colors.black87,fontSize: 14,fontWeight: FontWeight.normal),),

                  ],
                ),
                leading:ClipOval(

                    child:    Container(
                        width: 70.0,
                        height: 70.0,
                        decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(color: Colors.black87,width: 2),
                            image: new DecorationImage(

                                fit: BoxFit.fill,

                                image: ExactAssetImage(prodctus[index].image)
                            )
                        ))
                ),
                subtitle: Container(
                  margin: EdgeInsets.only(top: 10),

                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(prodctus[index].desc,style: textStyleView1(ColorUtils.colorButton, 12),),
                      Text(prodctus[index].timeValue,style: textStyleView1(Colors.grey.shade700, 12),),

                    ],
                  ),//child: Text("Woodland Shoes.Jhone\n#Paypal 525225 SEP-21",style: textStyleView1(AppUtils.colorBg, 14),),
                )

            )
        ),
      );
    });
  }
  static Widget loadTask(){
    return ListView.builder(
        shrinkWrap: true,

        itemCount:taskList.length,itemBuilder: (context,index){
      return Card(

        elevation: 0,

        child: Container(padding:EdgeInsets.all(5),
            child:ListTile(
              onTap: (){
               // Navigator.push(context, MaterialPageRoute(builder: (context)=>TaskDetails()));
              },
                title:Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(taskList[index].name,style: TextStyle(color: Colors.black87,fontSize: 14,fontWeight: FontWeight.bold),),
                    //  Text("\$17.47",style: TextStyle(color: Colors.black87,fontSize: 14,fontWeight: FontWeight.normal),),

                  ],
                ),

                leading:ClipOval(

                  child: Container(
                    height: 50.0,
                    alignment: Alignment.center,
                    width: 50.0,
                    color: getColor(index,taskList[index].timeValue),
                    child: Text(taskList[index].name.toString()[0],style: TextStyle(color: Colors.white,fontSize: 14,fontWeight: FontWeight.bold),),
                  ),
                ),
                subtitle: Container(
                  margin: EdgeInsets.only(top: 10),

                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [

                      Text(taskList[index].desc,style: textStyleView1(ColorUtils.colorButton, 12),),
                      if (taskList[index].timeValue.endsWith("Done"))
                        Text(taskList[index].timeValue,style:textStyleViewBold(Colors.green, 12) ,)
                     else if (taskList[index].timeValue.endsWith("Not Started"))
                        Text(taskList[index].timeValue,style:textStyleViewBold(Colors.deepOrangeAccent, 12) ,)
                      else Text(taskList[index].timeValue,style:textStyleViewBold(Colors.grey.shade700, 12) ,) ,
                    ],
                  ),//child: Text("Woodland Shoes.Jhone\n#Paypal 525225 SEP-21",style: textStyleView1(AppUtils.colorBg, 14),),
                )

            )
        ),
      );
    });
  }
  static Widget loadCustomer(){
    return ListView.builder(
        shrinkWrap: true,

        itemCount:customerList.length,itemBuilder: (context,index){
      return Card(
      color: ColorUtils.colorBflight,
        elevation: 0,

        child: Container(padding:EdgeInsets.all(5),
            child:ListTile(
              onTap: (){
             Navigator.push(context, MaterialPageRoute(builder: (context)=>ChatScreen()));
              },
                title:Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(customerList[index].name,style: WidgetUtils.textStyleView1(Colors.white, 14),),
                    //  Text("\$17.47",style: TextStyle(color: Colors.black87,fontSize: 14,fontWeight: FontWeight.normal),),

                  ],
                ),
                //trailing: Icon(Icons.arrow_forward_ios_outlined,size: 24,),
                leading:Container(
                  width: 60,
                  height: 60,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: AssetImage("assets/img/ic_user.jpg"),
                        fit: BoxFit.fill
                    ),
                  ),
                ),
                subtitle: Container(
                  margin: EdgeInsets.only(top: 10),

                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text("Hi how are you",style: textStyleView1(Colors.grey, 12),),
                 // /     Text(customerList[index].timeValue,style: textStyleView1(Colors.grey.shade700, 12),),

                    ],
                  ),//child: Text("Woodland Shoes.Jhone\n#Paypal 525225 SEP-21",style: textStyleView1(AppUtils.colorBg, 14),),
                )


            )
        ),
      );
    });
  }
  static Color getColor(int selector,String title) {

    if (taskList[selector].timeValue.endsWith("Done")) {
      return Colors.green;
    }
    else if ( taskList[selector].timeValue.endsWith("Not Started")) {
      return Colors.deepOrangeAccent;
    }
    else if (selector % 2 == 0){
      return Colors.blueGrey;
    }
    else {
      return Colors.blueGrey;
    }
  }
  static Color getColor1(int selector) {


     if (selector % 2 == 0){
      return Colors.blue;
    }
    else {
      return Colors.red;
    }
  }
  static  Widget myWidget(BuildContext context) {
    return GridView.builder(

        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
        ),
        itemCount: 10,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            margin: EdgeInsets.only(left: 10,right: 10,top: 5,bottom: 5),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            color:ColorUtils.colorBflight,
            child: Center(child: Container(

              child: Column(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.only(topRight: Radius.circular(10),topLeft: Radius.circular(10)),
                    child: Image.asset("assets/img/ic_user.jpg",height: 150,fit: BoxFit.cover,width: double.infinity,),
                  ),
                  SizedBox(height: 5,),
                  Text(
                    'Neha,',
                    style: WidgetUtils.textStyleView1(Colors.white, 14),
                  ),
                  Text(
                    'Active now ,',
                    style: WidgetUtils.textStyleView1(Colors.white, 12),
                  ),
                  //s   Image.asset("assets/img/ic_user.jpg",width: double.infinity,height: 200,fit: BoxFit.fill,)

                ],
              ),
            )),
          );
        }
    );
  }


}